import * as THREE from "./three/three.module.js";
// import { MapControls } from "./three/OrbitControls.js";
import { OrbitControls } from "./three/OrbitControls.js";
import { TTFLoader } from "./three/TTFLoader.js";
import { FontLoader } from "./three/FontLoader.js";
import { TextGeometry } from "./three/TextGeometry.js";
import { EffectComposer } from "./three/EffectComposer.js";
import { RenderPass } from "./three/RenderPass.js";
import { GlitchPass } from "./three/GlitchPass.js";
import { ShaderPass } from "./three/ShaderPass.js";
import { FilmPass } from "./three/FilmPass.js";
import { VignetteShader } from "../js/shaders/VignetteShader.js";

// Multilangue URL
let pathURL;
if (window.location.href.indexOf("/en/") != -1) {
  pathURL = "../";
} else {
  pathURL = "./";
}
//// Menu
const menuBtn = document.querySelector(".btn--menu");
const randomBtn = document.querySelector(".btn--random");
const menu = document.querySelector(".menu");
let menuOpen = false;
const musiqueBtn = document.querySelector(".btn--sound");
const musiqueBtnOn = document.querySelector(".btn--sound .info--on");
const musiqueBtnOff = document.querySelector(".btn--sound .info--off");
const morceau = document.querySelector(".son");

// const loadBtn = document.querySelector(".btn--load");
//
// loadBtn.addEventListener("click", (e) => {
//   e.preventDefault();
//   loadBtn.blur();
//   document.querySelector('.loading').classList.add("loading--done");
// });

menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuBtn.blur();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.enabled = false;
  } else {
    // if (!isTouchDevice()) controls.enabled = true;
  }
  menu.classList.toggle("show--menu");
});

// Musique
// let musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function() {
      if (track.volume < 0.99) {
        track.volume += 0.01;
      } else {
        clearInterval(fadeInAudio);
      }
    }, interval);
  } else {
    track.pause();
  }
}
let silence = true;
musiqueBtn.addEventListener("click", (e) => {
  e.preventDefault();
  musiqueBtn.blur();
  // if (silence) {
  //   musique(morceau);
  // } else {
  //   clearInterval(fadeInAudio);
  //   clearInterval(fadeOutAudio);
  //   morceau.pause();
  // }
  // musiqueBtnOn.classList.toggle("none");
  // musiqueBtnOff.classList.toggle("none");
  // silence = !silence;
  // // musiqueBtn.classList.toggle('playing');
  // scene.remove(textMesh);
  // objectVisible = listObjects[randomNb(0,(nbObjects - 1))];
  // scene.add(objectVisible);
    const objload = document.querySelector('.objload');
    objload.classList.add('objload--anim');
    objload.addEventListener('animationend', () => {
      objload.classList.remove('objload--anim');
    });

    const objloadvert = document.querySelector('.objload-vert');
    objloadvert.classList.add('objload-vert--anim');
    objloadvert.addEventListener('animationend', () => {
      objloadvert.classList.remove('objload-vert--anim');
    });
  addTextToScene();
});

function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomNbFrac(min, max) {
  // min and max included
  return Math.random() * (max - min + 1) + min;
}

function isTouchDevice() {
  return (
    "ontouchstart" in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0
  );
}

//
// Three.js
//
let container;
let camera, controls, scene, renderer;
let starBox, stars, vertices;
let ambientLight, dirLight1, dirLight2, spotLight;
let dirGroup;
let displayText = [];
let textMeshes = [];
// let group;
let parameters;
// let materials = [];
let composer, composer2;
let clickable = true;
let movingCamera = false;
let movingCameraEnd = false;
let animClick = false;
let loadingTexture1 = false;
let loadingTexture2 = false;
let loadingTexture3 = false;
let loadingTexture4 = false;
let loadingTexture5 = false;
let loadingTexture6 = false;
let loadingTexture7 = false;
let loadingTextureGround = false;
let loadingBlocks = false;
let everythingLoaded = false;

const textes = [...document.querySelectorAll(".textes p")];
const verses = [];
textes.forEach((el) => {
  const innerText = el.innerHTML;
  verses.push(innerText);
});
const texteContainer = document.querySelector(".textes");

let listSolids = [];

const mouse = new THREE.Vector2();
const clock = new THREE.Clock();

init();
//render(); // remove when using next line for animation loop (requestAnimationFrame)
animate();

function init() {
  scene = new THREE.Scene();

  // Colors
  scene.background = new THREE.Color(0x011001);
  scene.fog = new THREE.Fog(0x011001, 1, 700);

  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    4000
  );
  camera.position.set(0, 100, 200);

  // lights
  // const dirLight = new THREE.DirectionalLight(0xffffff, 0.125);
  // dirLight.position.set(0, 0, 1).normalize();
  // scene.add(dirLight);

  // const pointLight = new THREE.PointLight(0xffffff, 1.0);
  // pointLight.position.set(50, 100, 90);
  // pointLight.castShadow = true;
  // pointLight.shadow.camera.near = 8;
  // pointLight.shadow.camera.far = 200;
  // pointLight.shadow.mapSize.width = 512;
  // pointLight.shadow.mapSize.height = 512;
  // pointLight.shadow.bias = -0.002;
  // pointLight.shadow.radius = 4;
  // scene.add(pointLight);
  // const pointLight2 = new THREE.PointLight(0xffffff, 1.0);
  // pointLight2.position.set(-50, 100, -90);
  // scene.add(pointLight2);

  ambientLight = new THREE.AmbientLight(0x5ba300, 0.4);
  scene.add(ambientLight);

  spotLight = new THREE.SpotLight(0xffddff);
  spotLight.angle = Math.PI / 5;
  spotLight.penumbra = 0.3;
  spotLight.position.set(120, 30, 120);
  spotLight.target.position.set(0, 0, 0);
  spotLight.castShadow = true;
  spotLight.shadow.camera.near = 8;
  spotLight.shadow.camera.far = 2000;
  spotLight.shadow.mapSize.width = 512;
  spotLight.shadow.mapSize.height = 512;
  spotLight.shadow.bias = -0.002;
  spotLight.shadow.radius = 4;
  scene.add(spotLight);
  scene.add(spotLight.target);

  dirLight1 = new THREE.DirectionalLight(0x78be20, 0.5);
  dirLight1.position.set(200, 200, 200);
  dirLight1.target.position.set(0, 0, 0);
  dirLight1.castShadow = true;
  dirLight1.shadow.camera.near = 1;
  dirLight1.shadow.camera.far = 1200;
  dirLight1.shadow.camera.right = 400;
  dirLight1.shadow.camera.left = -400;
  dirLight1.shadow.camera.top = 550;
  dirLight1.shadow.camera.bottom = -150;
  dirLight1.shadow.mapSize.width = 1024;
  dirLight1.shadow.mapSize.height = 1024;
  dirLight1.shadow.radius = 4;
  dirLight1.shadow.bias = -0.0009;
  scene.add(dirLight1);
  scene.add(dirLight1.target);

  // spotLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
  // spotLight.position.set( 60, 60, 60 );
  // spotLight.target.position.set( 0, 0, 10 );
  // scene.add( spotLight );
  // scene.add( spotLight.target );
  // console.log(spotLight.target)

  // dirLight2 = new THREE.DirectionalLight(0xe0c69f, 0.5);
  // dirLight2.position.set(100, 100, -100);
  // dirLight2.castShadow = true;
  // dirLight2.shadow.camera.near = 1;
  // dirLight2.shadow.camera.far = 400;
  // dirLight2.shadow.camera.right = 200;
  // dirLight2.shadow.camera.left = -200;
  // dirLight2.shadow.camera.top = 150;
  // dirLight2.shadow.camera.bottom = -150;
  // dirLight2.shadow.mapSize.width = 1024;
  // dirLight2.shadow.mapSize.height = 1024;
  // dirLight2.shadow.radius = 4;
  // dirLight2.shadow.bias = -0.0009;
  // scene.add(dirLight2);

  // dirGroup = new THREE.Group();
  // // dirGroup.add(dirLight1);
  // dirGroup.add(dirLight1);
  // dirGroup.add(dirLight1.target);
  // // dirGroup.add(dirLight2);
  // scene.add(dirGroup);

  // const dirLight = new THREE.DirectionalLight(0xffffff, 0.125);
  // dirLight.position.set(0, 0, 1).normalize();
  // scene.add(dirLight);

  // const pointLight = new THREE.PointLight(0xffffff, 1.5);
  // pointLight.position.set(0, 100, 90);
  // scene.add(pointLight);

  // const helper = new THREE.CameraHelper(dirLight1.shadow.camera);
  // scene.add(helper);
  // const helper2 = new THREE.CameraHelper(dirLight2.shadow.camera);
  // scene.add(helper2);

  // const axesHelper = new THREE.AxesHelper(5000);
  // axesHelper.setColors(0xff0000, 0x00ff00, 0x0000ff);
  // scene.add(axesHelper);

  // const helperLight = new THREE.DirectionalLightHelper(dirLight1, 5);
  // scene.add(helperLight);
  // const helperLight2 = new THREE.DirectionalLightHelper(dirLight2, 5);
  // scene.add(helperLight2);


  // group = new THREE.Group();
  // group.position.y = 0;
  //
  // scene.add(group);

  const planeGeometry = new THREE.PlaneGeometry(10000, 10000);
  const planeMaterial = new THREE.MeshPhongMaterial({
    color: 0x5ba300,
    // emissive: 0x78be20,
    specular: 0x5ba300,
    shininess: 10,
  });

  const ground = new THREE.Mesh(planeGeometry, planeMaterial);
  ground.rotation.x = -Math.PI / 2;
  // ground.scale.multiplyScalar(3);
  ground.castShadow = true;
  ground.receiveShadow = true;
  scene.add(ground);

  addTextToScene();

  const finish = document.querySelector(".loading");
  finish.classList.add("loading--all-is-loaded");

  // Render
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.VSMShadowMap;

  // controls
  controls = new OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.screenSpacePanning = false;
  // controls.panSpeed = 0.2;
  // controls.rotateSpeed = 0.8;
  // controls.zoomSpeed = 0.8;
  controls.minDistance = 0;
  controls.maxDistance = 800;
  controls.maxPolarAngle = Math.PI / 2.1;
  controls.target = new THREE.Vector3(0, 1, 0);
  controls.update();

  // Container
  container = document.getElementById("container");
  container.appendChild(renderer.domElement);

  // postprocessing
  const shaderVignette = VignetteShader;
  const effectVignette = new ShaderPass(shaderVignette);
  // const glitchPass = new GlitchPass();
  // glitchPass.goWild = true;

  effectVignette.uniforms["offset"].value = 0.9;
  effectVignette.uniforms["darkness"].value = 1.75;
  //
  const effectFilm = new FilmPass(0.3, 0.05, 800, false);
  //
  // // Glitch
  // composer = new EffectComposer(renderer);
  // composer.addPass(new RenderPass(scene, camera));
  //
  // composer = new EffectComposer(renderer);
  // composer.addPass(new RenderPass(scene, camera));
  //
  // composer.addPass(glitchPass);
  // composer.addPass(effectFilm);
  // composer.addPass(effectVignette);
  //
  // // Permanent
  composer = new EffectComposer(renderer);
  composer.addPass(new RenderPass(scene, camera));

  composer.addPass(effectFilm);
  composer.addPass(effectVignette);

  window.addEventListener("resize", onWindowResize);
}

async function removeTextFromScene() {
  textMeshes.forEach((el) => {
    scene.remove(el);
  });
  return;
}

async function addTextToScene() {
  await removeTextFromScene();
  textMeshes = [];
  displayText = [];
  const longueur = verses.length;
  const nbVerses = 10;
  let posy = (20 * nbVerses) - 5;
  const depart = randomNb(0, longueur - nbVerses);
  for (let i = 0; i < nbVerses; i++) {
    displayText.push(verses[depart + i])

  }
  const loader = new FontLoader();
  loader.load("js/fonts/plex-mono.json", function(font) {
    for (let i = 0; i < displayText.length; i++) {
      let textGeoNew = new TextGeometry(displayText[i], {
        font: font,
        size: 8,
        height: 100,
        curveSegments: 12,
        bevelEnabled: false,
        bevelThickness: 0,
        bevelSize: 0,
        bevelOffset: 0,
        bevelSegments: 0
      });

      textGeoNew.computeBoundingBox();
      textGeoNew.center();

      // const centerOffset = - 0.5 * (textGeoNew.boundingBox.max.x - textGeoNew.boundingBox.min.x);

      const materials = [
        new THREE.MeshPhongMaterial({
          color: 0xffffff,
          emissive: 0xddffdd,
          // specular: 0xaa0000,
          shininess: 100,
        }), // front

        new THREE.MeshPhongMaterial({
          color: 0xffffff,
          // emissive: 0xaa0000,
          specular: 0x002800,
          shininess: 100,
        }), // side
      ];
      let textMesh = new THREE.Mesh(textGeoNew, materials);

      textMesh.position.x = 0;
      textMesh.position.y = 0 + posy;
      textMesh.position.z = 0;

      posy = posy - 20;

      // textMesh1.rotation.x = 0;
      // textMesh1.rotation.y = Math.PI * 2;

      textMesh.castShadow = true;
      textMesh.receiveShadow = true;

      textMesh.speedAnim = randomNbFrac(1, 10);

      textMeshes.push(textMesh);


    }
    textMeshes.forEach((el) => {
      scene.add(el);
    });
    render();
  })
}

let timer;
controls.addEventListener("start", (el) => {
  movingCameraEnd = false;
  // timer = setTimeout(() => {
  //   moveControls = true;
  // }, 150);
});

controls.addEventListener("end", (el) => {
  clearTimeout(timer);
  movingCameraEnd = true;
  if (movingCamera) {
    movingCamera = false;
  }
});

controls.addEventListener("change", (el) => {
  if (movingCameraEnd) {
    clearTimeout(timer);
    movingCamera = false;
  } else {
    timer = setTimeout(() => {
      movingCamera = true;
    }, 150);
  }
});

container.addEventListener("click", onDocumentMouseClick, false);
container.addEventListener("mousemove", onDocumentMouseMove, false);
texteContainer.addEventListener(
  "mousemove",
  (event) => {
    event.stopPropagation();
    document.body.style.cursor = "";
  },
  false
);

let textesOpen = false;
let timerGlitch;
function onDocumentMouseClick(event) {
  const mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  if (intersects.length > 0 && clickable) {
    clearTimeout(timerGlitch);
    const texteChoisi = textes[randomNb(0, textes.length - 1)];
    document.body.style.cursor = "";
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      texteChoisi.classList.add("show");
      document.querySelector(".texte.show").scrollIntoView();
    } else {
      texteContainer.classList.add("show");
      texteChoisi.classList.add("show");
      // if (!isTouchDevice()) controls.activeLook = false;
      textesOpen = !textesOpen;
    }
    animClick = true;
    timerGlitch = setTimeout(() => {
      animClick = false;
    }, 2000);
  }
}

const closeBtn = document.querySelector(".textes__close");
closeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".texte.show").scrollIntoView();
  texteContainer.classList.remove("show");
  textes.forEach((el) => {
    el.classList.remove("show");
  });
  textesOpen = !textesOpen;
  // if (!isTouchDevice()) controls.activeLook = true;
  closeBtn.blur();
});

function onDocumentMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";
  } else {
    document.body.style.cursor = "";
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate(time) {
  requestAnimationFrame(animate);

  textMeshes.forEach((el) => {
    el.rotation.y += 0.0001 * el.speedAnim;
    // el.rotation.x += 0.01;
    // el.rotation.y += 0.01;
    // el.rotation.z += 0.01;
  });

  controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true
  // let x = spotLight.position.x;
  // let z = spotLight.position.z;
  // let theta = 0.002;
  // spotLight.position.x = x * Math.cos(theta) + z * Math.sin(theta);
  // spotLight.position.z = z * Math.cos(theta) - x * Math.sin(theta);

  let x = spotLight.position.x;
  let z = spotLight.position.z;
  let theta = 0.002;
  spotLight.position.x = x * Math.cos(theta) + z * Math.sin(theta);
  spotLight.position.z = z * Math.cos(theta) - x * Math.sin(theta);

  render();

  // composer.render();

  // if (
  //   !everythingLoaded &&
  //   loadingTexture1 &&
  //   loadingTexture2 &&
  //   loadingTexture3 &&
  //   loadingTexture4 &&
  //   loadingTexture5 &&
  //   loadingTexture6 &&
  //   loadingTexture7 &&
  //   loadingTextureGround &&
  //   loadingBlocks
  // ) {
  //   everythingLoaded = true;
  // }

  // if (movingCamera) {
  //   clickable = false;
  // } else {
  //   clickable = true;
  // }

  // for (let i = 0; i < vertices.velocities.length; i++) {
  //   vertices.velocities[i / 3 + (i % 3)] += vertices.accelerations[i];
  //   vertices.positions[i * 3 + 1] -= vertices.velocities[i];
  //
  //   if (vertices.positions[i * 3 + 1] < -200) {
  //     vertices.positions[i * 3 + 1] = 4000;
  //     vertices.velocities[i / 3 + (i % 3)] = 0;
  //   }
  // }

  // stars.rotation.y += 0.002;

  // starBox.setAttribute(
  //   "position",
  //   new THREE.BufferAttribute(new Float32Array(vertices.positions), 3)
  // );

  // const delta = clock.getDelta() / 20;

  // dirGroup.rotation.y += 0.7 * delta;
  // dirLight1.position.z = Math.cos(time * 0.01);


  // renderer.render(scene, camera);

  // if (animClick) {
  //   composer.render();
  // } else {
  //   composer2.render();
  // }
}

function render() {
  renderer.render(scene, camera);
}

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector(".menu"));
let simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
// Disable scroll simplebar for print
window.addEventListener("beforeprint", (event) => {
  simpleBarContent.unMount();
  simpleBarContentTextes.unMount();
});
window.addEventListener("afterprint", (event) => {
  simpleBarContent = new SimpleBar(document.querySelector(".menu"));
  simpleBarContentTextes = new SimpleBar(document.querySelector(".textes"));
});

//
// Scripts hack
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

////
//// Prevent bounce effect iOS
////
//// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
//// let contentBounce = document.querySelector('.container');
//// contentBounce.addEventListener('touchstart', function (event) {
////   this.allowUp = this.scrollTop > 0;
////   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
////   this.slideBeginY = event.pageY;
//// });

//// contentBounce.addEventListener('touchmove', function (event) {
////   let up = event.pageY > this.slideBeginY;
////   let down = event.pageY < this.slideBeginY;
////   this.slideBeginY = event.pageY;
////   if ((up && this.allowUp) || (down && this.allowDown)) {
////     event.stopPropagation();
////   } else {
////     event.preventDefault();
////   }
//// });

// //
// // Old iOS versions
// //
// let messageIOS = false;
//
// const platform = window.navigator.platform;
// const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
//
// if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
//   const banner = document.createElement('div');
//   banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">This page will not work properly with iOS versions older than 13.<br>(Click on this banner to make it disappear.)</p></div>`;
//   banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
//   document.querySelector('body').appendChild(banner);
//   messageIOS = true;
// }
