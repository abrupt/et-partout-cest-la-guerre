---
title: Et partout c'est la guerre
---

pas de nom

pas besoin de nom

ou alors un

inventé de toutes pièces

qui ne reposerait sur rien

que de l'air ---

pas besoin de noms

trop de silences les entourent

silences qui accomplissent

le destin des noms

de ne rien signifier du tout

musique difficile

un coin de gris apocryphe

je le regarde sans le voir

absorbé par la myopie du ciel

il coule un peu là

un peu plus haut

aussi

après la pluie

et la musique difficile

le nez contre la vitre

tu ne la vois peut-être pas

peut-être que le paysage

ou ce qu'il en paraît

le nez contre la vitre

tu ne la vois peut-être pas

ce qui ne veut pas dire

qu'elle disparaît

posologie de la fin du monde

formes toutes faites qui diluent

les idées noires ---

doses de mort quotidienne ---

elles coulent imprègnent réduisent

lamento au bout du temps

tu es à toi-même ta propre éclaircie

question de lumière

jusques à travers les nuages

clandestins

et secrets

musique difficile

et périlleuse comme la nuit

qui s'y risque s'y perd

oublie son nom

coordonnées sans repère

disparition de l'identité

musique difficile

qui s'y refuse

pourrait aussi bien ne pas exister

rien à haïr ce matin

peut-être que le monde est devenu meilleur

de la nuit au lendemain

ou bien la vie sociale s'est-elle éteinte

faute d'électricité ---

j'allume la lumière

et tout redevient normal

relativement mal

musique difficile

échos du désastre

jusque dans le silence

et puis jeux de langage

avec des astres

il faudrait pour composer une vie ---

une vie meilleure sans doute ---

écrire comme la musique

vivre pour le rythme

et la règle

nécessaire de survie

le bleu pur du ciel

la ligne d'horizon

là où il se confond

avec la mer

le vent qui claque dans l'air

et sur le visage

décongestionne l'esprit

le libère de la grisaille

dans le crépuscule infini

en écho à la mer

l'olivier tremble

le vent souffle

de là où je suis assis

c'est tout ce que je vois

et le ciel derrière

et la mer dessous

et les îles dessus

frioul et if

if récit

si récif

sur le balcon

cet après-midi

rien à haïr

non plus

tout ce que cachent les noms

propres

tout l'espace entre les choses figées

par le temps

les objets

morts

de notre peur instable de vieillir

et ne pas pouvoir ne pas

grammaire d'un verbe qui se conjugue

quand même dans la plus grande solitude

ignorance totale

la plus anonyme des perfections

grammaire d'un verbe qui se conjugue

tout le temps ---

présent semper

je ne peux pas ne pas

et nul besoin de nom

pour ça

quel jeu pour quelle langue

quel feu pour quel monde

quel corps pour quel corps

qui mieux que personne ?

nous

qui n'aimons rien tant que

les questions sans réponses

rien tant que

les questions pour répondre

aux questions

avec l'impression vague d'avoir quelque chose

à dire

je fais des suites

sur le bout de la langue

nombre indéterminé de notes

pas de variations

(pas de thèmes item)

pas de répétitions

la mer

soleil dès le matin

et jusque si tard dans la nuit

toujours la même

musique difficile

l'espace blanc

l'espace d'un instant

découvrir qu'il n'y a pas

de traces

les déchiffrer par suite

ou bien esprit de contradiction

il faut s'arrêter

et

il ne faut pas s'arrêter

le supplice est

l'indice du vrai

ce que veut toute

musique difficile

et plus encor

tenir captives

les âmes qui ne croient plus aux âmes

les muses qui ne croient plus aux muses

les dieux qui ne croient plus aux dieux

monter chaque pas d'un degré

sans savoir où aller

il n'y a plus nulle part où aller

qu'ici

gestes des mains

restes de rien

quand la terre disparaît à l'horizon

simagrées

images encadrées

foule qui fonce contre son gré

foule aux pieds

d'aucuns

en retrait fomentent

un complot

une farce

nul donc ne sait

tout mouvement n'est pas un geste

des yeux

impassible

gré contre guerre

essais

échecs

extases

là où n'est pas

la vérité

entre l'obsolescence et

le posthume

la musique difficile

ne fait pas

l'obole du sens

comme au ciel

rien que le ciel

dans le ciel

idem

bleu couleur

de lui-même

preuve de l'existence

d'un matin parfait

irréfutable

de l'hiver à l'envers

moins que l'hémisphère

et

pas un mot sur le langage ---

traître ---

à coups de coup du sort

déceler un ordre

ou quelque combinaison

possible

j'ai des iris plein les yeux

dit ma planète

imaginaire

l'éden n'est pas le jardin

mais

la flore partout

tout est là

l'oubli du passé

comme forme du futur

plus rien à déchiffrer ---

défricher sans ombre pour abri

telle ambivalence des sentiments

ne plus savoir

que haïr

science malaimée

tout est là

bavard

comme le nom de quelque chose

qu'on donne

quand elle n'existe pas

tout est là

que tu l'appelles

mutinerie

ou bien

mutisme

ou bien

n'importe quoi

illumination

point d'exclamation

mais

pas le sens

ni le son ---

lumière

en avant

et

en haut

dans la boîte crânienne

lumière

tirant sur le jaune

tirant sur l'ailleurs

force quand même il n'y a plus rien

qu'on dirait

vivre

sans doute

des ensembles vides

nul le nom

seul le souffle

la mémoire en somme

une certaine forme

de l'âme défunte

feue l'âme donc

laisse-la brûler

et

tout ce qu'elle contient

avec

nul le nom

annulé comme

l'entité qu'il nomme

l'unité qu'il somme

nul le nom

nul besoin

de se cacher

dedans

des ensembles livides

tours

barres

mondes qui grimpent

ascension ?

peut-être pas

des ensembles liquides

europe

le monde a oublié ton nom

nous sommes seuls ---

pas la solitude ---

le seul

un comme île

elle et moi

après nous

il n'y eut plus jamais

d'ailleurs

or ils crurent vivre sans carcan

consignera l'épitaphe sans personne

pour la lire

d'où le non nom de l'être

peut-être

europe

enlèvement du nom propre

rien ne disparaît

transparence irrémissible

corps sans agents

foule d'anonymes

coupables sans aveux

sans même le crime

tout s'efface

jeux avec le mot

mort

rien ne disparaît

devient métaphore

dès lors

et puis pourrit

tout les lasse

sauf le nom

vivons dans les traits de l'espace

pas les séquelles de ce qui a eu lieu

déjà passé

affres en suite

l'histoire n'est pas la même

il y a toujours quelque chose à raconter

dont nul ne se souvient

non déjà existé

nuage de poussière

de cendre aussi

pages

rongées par les vers

et

mises au feu par les hommes bûchers

honnêtes

comme des hordes d'analphabètes

en longues processions

génuflexions invisibles

toutes à l'intérieur

et

pas une lettre qui les sauve

tous ---

ils ont oublié

la musique difficile

qui consume les entrailles

de qui elle se laisse comprendre

l'existence

d'une couleur dominante

n'implique pas

l'existence

d'une couleur dominée

bleu tirant sur

la méditerranée

(corollaire ---

la mer n'est pas une mère)

nous ---

descendants de personne

de la lignée du grand ironiste

marins à la dérive

amoureux captifs

musiciens au grand air

nous

qui savons nous passer d'un nom

pas de deux de l'histoire

la rature est notre triomphe

traces de pas dans la neige

j'essaie d'oublier celles

si nommées qu'innommables

qui me précèdent ---

oh l'impossible tâche ---

traces de pas sur la plage

là même où la neige

dessous

le sable intact

non foulé

traces de pas sur la plage

celles que je peux dire

les miennes

et ne pas pouvoir ne pas

écrire quitte

pour soi ---

le seul qui aille ---

l'experimentum crucis ---

écrirais-je encor

quand même

personne ne me lirait

jamais

est-ce une affirmation ?

la singularité qui traverse

détruit

c'est-à-dire

toutes les subjectivités

anonymes comme innommables

pas une invitation au silence

peut-être une autre

science

les ondes

sans les choses

les mondes

sans les poses

personne

ne se noiera plus jamais

dans l'étang de l'être ---

périmé

des ensembles liquides

je n'ai pas de mémoire ---

zéro octet ---

j'ai tout oublié

et ne veux me souvenir de rien

sang

mort

ordure anecdotique

métaphore

autorité

punition

châtiment

honte

culpabilité

je veux qu'on ne me rappelle rien

l'innocence c'est moi

l'innocence

c'est moi

le destin

c'est moi

l'avenir que rien n'attend

c'est moi

les rendez-vous manqués

c'est moi

avec l'histoire

c'est moi

sans histoire

c'est moi

le monde qui tourne en rond autour de son axe déconcentre bascule s'effondre et s'achève ruine hideuse

c'est moi

l'oubli de l'être

c'est moi

la racine du mal radical

c'est moi

sa fin

aussi

c'est moi

le soleil pâle de la raison

humiliée

brille encore

quelquefois

à la fin d'une journée

rien que l'idée te donne envie de fuir

ne pas continuer

ne plus jamais continuer

sans mourir pourtant

non

comme le goût de l'impossible

est suave

sur le bout de la langue

et si tu ne pouvais plus parler ?

on a toujours quelque chose à dire

tout le monde a toujours quelque chose à dire

sauf le silence

rien que l'idée te donne envie de te taire

l'idée d'avoir une idée

peut-être

mais il faut encore parler

encore faire semblant

un jour de plus

et caetera

jusques au bout de la vie

l'absence d'envie

n'est pas une excuse

pas une raison

suffisante

pourtant

depuis quand

faire toutes les choses ---

et toutes ces choses ---

parler se taire continuer ---

ne sert-il plus à rien

des années

depuis toujours ?

des ensembles livides

l'esprit la nuit

la machine à écrire

tout est muet

mais tout n'est pas gris

le carnet seul

noire

la nuit l'esprit devient

la machine à écrire

le jour

rien n'a d'importance

que cette nuit

et

ce jour

tout le reste ---

des ensembles vides

mon désert domestique

sauvage comme une image

mon désir élastique

lisse comme l'asphalte

chaque fois que le monde

fait peau neuve

prendre la route

quand même tu saurais où aller

quelque chose pourrait se passer

surgir hors de la carte

te jeter ailleurs

dans le soleil ou dans la mer ---

mon désir domestique

de demeures élastiques

parcourt les ruines civilisées

les zones trop courues

où il n'y a plus rien à voir

et

où l'on peut dès lors

s'aveugler à peu de frais

devenir

s'il est possible

un être un peu moins fait ---

une puissance pour l'avenir

rien ne nous sépare de rien

de la couleur identique

nuances de ciel et de mer

le vent devient la tempête

qu'il chasse

alerte bleue

sur les deux hémisphères

de mon cerveau

dévorer le ciel

mâchoires

falaises

foutaises

arbres dans le néant

sans plus de racines

des forêts d'hier

avaler la mer

ou bien encore

ce qu'il y a derrière

et enfin

se dissoudre

dedans

mal né

fils de quiconque

figure pâle

dans une raie de lumière

on l'y devine

quand même

elle ne serait pas là

ne ferait que circuler

reflet de l'éternité

désespoir du temps qui passe

mal né

ou seulement à peine

échos d'antan dans un pays sage

les filles

elles

auront plus de chance

peut-être

à droite

ou à gauche

le sel monte aux lèvres

comme le soleil dans les yeux

à midi

reflets aveugles sur la mer

bouche vaste

de la méditerranée

pas lasse

non

affable

simplex sigillum nenni

musique difficile

bien avant le son

embruns jusques aux nuages

ciel où sont tous les paysages

quelques notes enfin

éclats de lumière

fracassent les esprits

vagues

désirer le soleil

tout

le feu de la langue

brûler

et

dans le refuge de l'ombre

chasser

l'obscurcie

l'enfant illuminé

découvre

les couleurs du prisme ---

regarde

trop de vies

sans spectres ni hantises

trop de vies

sans images d'elles-mêmes

trop de vies

sans personne pour les vivre

dedans

les différences qui n'en sont pas

les différences qui n'en font pas

la mappemonde

la route

boucle

apparition de l'infini

au pas de la porte

la roue

le drone

le disque

tout tourne

spirale

qui nous guide

dans la disparition du paysage

moi et monde s'estompent

les différences qui n'en font pas n'en sont pas

les deux hémisphères

de mon cerveau

l'espace se dilate

vertige sur place

les pieds sur terre

et

la tête dans les nuages

le nez en l'air

aussi

peut-être

histoire

non de trouver quelque chose supérieure

mais

d'aller voir ailleurs

les rides sont nos réponses aveugles à

l'inquiétude du soleil

le chant assourdissant de la chaleur que

tu pressens

déjà

à la fin de l'hiver

même après l'été

ne l'oublie pas

le lyrisme est une histoire d'insectes

saborde-toi

détruis les mondes qui sont vains ---

populace en nombre infini ---

trop de vies prêt-à-porter ---

illusion de l'altérité ---

qu'il est sidérant

ce vide

pas profond du tout

non

langue plate qui s'étend

bien plus loin encor

que le fond

de l'horizon

se noyer dans le ciel bleu

ne crois pas la vie simple

facile

il faut inventer la faille

apprendre à sentir

apprendre à se déprendre

prendre congé

les illusions sont des jeux de lumière

plutôt l'air que la vérité ---

quelque chose comme

précisément

le bleu du bleu du ciel

il faut investir la faille

investir dans la broussaille

des écailles de dieu sur la peau des vivants

signes muets adressés à tout le monde

langage débonnaire de l'énormité

réaction qui triomphe en devenant progrès et puis

pas silencieux dans le bleu tout autour

si tu coules c'est que tu appartiens

apprends à flotter

sur la terre comme au ciel

quand la neige aura recouvert les écailles de dieu

et le soleil tout brûlé ensuite

auras-tu accompli

un rêve

un drame

quelque révolution

ou te seras-tu contenté

de regarder le temps qui passe

passer ---

un peu triste

un peu gris ?

tout le sang du néant

goutte sur langue

ruisselle

rappelle

il n'y a pas de barrage sur l'obscène --- rien

que le goût amer de la défaite

sans le nom pas sans le sang

pulsation base chaque respiration

vitesse du vivant

historique des variations

sans le nom pas sans le son

gicler

partout où tu tais ton nom

quelque chose croît

sans le nom pas sans le sang

du plus au moins et retour

changement

la seule histoire à raconter

quelqu'un est mort qui continue de vivre

sans plus personne qu'il hante

tout le monde mort aussi

ou impuissant

ou fatigué

ou indifférent

n'est-ce pas la même chose

après tout ?

tous les matins

sont les mêmes

tu accueilles les nouvelles

d'un œil vide

il n'y a rien pour toi

rien pour personne non plus

dans l'œil quelque chose pourrait briller

mais non

c'est fini ---

vide

dans l'œil dehors partout

chaque jour est le même

il n'y a rien

pour personne

et

tu es le seul qui le voies

quelqu'un est mort qui continue de vivre

fantôme littéral de soi-même

qui dira

les larmes séchées

les sommeils lourds

la peau brûlée

la haine du réveil

quelque chose comme

le soleil voilé

dont la lueur morne

rappelle l'éternité

qui dira

toutes nos antiquités

appels étouffés

angoisses nocturnes

d'où l'on ne se lève

jamais ?

et après que tu l'auras dit

qu'en feras-tu ?

les gens sont fous

mon amour

ai-je pensé la regardant

dormir

quelques instants à peine

les gens sont fous

mon amour

ou bien

c'est ce que j'aurais voulu lui dire

alors

mais il aurait fallu la réveiller

alors

je n'ai rien fait

je l'ai simplement regardée

dormir

quelques instants de plus

les gens sont fous

mon amour

silence

fatigue sur les yeux

sifflements

vent

ou acouphènes

on ne peut pas décider

entre loin et proche

le bruit des travaux

de l'autre côté de l'avenue

comme un marteau-piqueur dans le ciel

qui décime

un

à

un

les nuages

quand as-tu pour la première fois eu le sentiment de ne pas appartenir à ce monde ?

as-tu seulement un jour eu le sentiment d'appartenir à un monde ?

la fatigue sur les yeux m'empêche

je crois

de chercher dans mes souvenirs

les éléments d'une réponse

ou d'une autre

la chose du désir

sans escarre aucune

mais

pas le désir d'une chose ---

police occidentale ---

ni ce qui du désir rend chose

pierre de mer

méduse

étoile

non

un peu plus loin

précisément

au glissement du terrain

la chose du plaisir

ensuite

pour elle

se faire tout ouïe

cris d'oiseaux dans la nuit

ils disent

la mer est proche

et la fin de tout

aussi

cris d'oiseaux dans la nuit

pas un chant

ni une plainte

un organe

qui exprime

l'air de l'eau

où ils retournent

aller et venir

voler et mourir

cris d'oiseaux dans la nuit

je ne les entends plus

un organe

c'est-à-dire

un orgasme

crispent le monde

les mains

sur lui

sinon le nom peine

ne touche rien

pas la langue

qui fouille

la chair du dit

monde

ne tient à rien

la langue

ne rime avec rien

le vent qui souffle

ne me dévie pas d'un pouce

c'est lui

mon oiseau futur

à mort gutenberg

et ta portée de crétins reproducteurs

armée de nains identiques

qui se pavanent

comme s'ils l'étaient

uniques

tripotée du même

faces blêmes

triomphe de l'uniformité

crève

rejeton du père idem

rien que pareil

c'est ainsi que tu l'aimes

et puis

longue vie à la vie

photocopie

et

autres tautologies

à mort

esprit de l'imprimerie

métadonnées

ou sorcellerie

on aurait pu aller voir ailleurs

tu sais

les mains dans les poches

flâner tu sais

au lieu de quoi

décimés car multipliés

à la course

battus

on nous a vendus

théorie d'onomatopées

pour des vérités fantoches

plus tard

il y aura des nuages

suspendus au hasard

tout monde est une puissance d'effondrement

un corps mort ne tombe pas nécessairement

on l'oublie simplement

on oublie tout ---

si simplement

plus guère que le goût de la défaite

larmes de gloire

sanglots salés

de la terre dans la bouche

que tu ravales

plus guère que le goût amer de la défaite

illusoire rancune

des nuées d'insectes

autour des écrans

et leur lune infecte

qui brille

dans le néant

seul le goût amer de la défaite

peut te porter à

l'arme de paix

mourir ou décélébrer

oublier tout l'immonde

et

espérer l'être

comme tout le monde

quelque chose vibre

et

on n'entend rien

quelque chose craque

et

tout le monde tombe ---

c'est le bruit que fait le bien

extases ou métastases ---

il y a des rats qui courent sur le plancher

sont-ils devenus insensibles ?

surtout ne réponds pas

il y a des rats qui courent sur le plancher

il ne suffit plus de les écraser

utopies et oiseaux

des mensonges écrits en lettres de sang

ne sont pas plus vrais que sans

vol d'oiseaux exotiques

à la fenêtre

traînées vertes

dans le ciel bleu

pourquoi des couleurs seraient-elles primaires ?

et si elles le sont qu'est-ce qu'il vient après ?

des primitifs ?

ils sont si loin

nos rêves

oiseaux magiques

à tout bout de champ

ils sont si loin nos rêves

le matin au réveil

tu te frottes les yeux

utopiques

mais plus personne ne veut

de tes cieux

des enfants idiots jouent sur la plage

à la guerre à la mort

à se battre à se jeter des pierres

filles et garçons

tous du même côté

les pères absents

au mieux

sur la grève

visage masqué

quelques mères

derrière le voile des apparences

crient des horreurs

que personne ne comprend

les histoires n'ont pas de sens

c'est ainsi qu'on les reconnaît

tous les empires

emprise sur ton esprit

le monde que tu ne vois pas

seulement ton doigt

du bout duquel tu montres

des ombres

des spectres qui se prennent pour toi

des formes vagues

des desseins confus

au lieu de terrains nus

irrécupérables

des musées tristes comme l'idée

que l'on se fait de la pierre

place vacante du saint père

et

que tu t'imagines combler

de frasques

plutôt que de fables

tout sauf le nom

ne m'appelle pas

non

laisse-moi flotter

errer

laisse-moi

ne pas avoir l'air d'être

laisse-moi dérangé

quelque part où personne

n'est allé

qui fut nommé

faire sur le monde

des nuées de points

et tourner autour

les alentours

et tout ce qui

désarticule

langue déliée

ne craint pas de manquer

à la vérité

en vérité

qu'est-ce que la vérité ?

faire pour tout le monde

une infinité de points

et circuler

passer par tous

passer partout

en vérité

où commence la vérité ?

pluie sur la route

et rien d'autre

que de la musique

elle étouffe

là mais absente

comme toute chose

qui se meurt

d'ennui

nous fait mourir

grandes eaux

et fortes

au contraire

de la musique

facile

et

qui tourne

tout en dérision

pas de sens

pluie sur la route

le soleil droit devant

coule au bout de ces doigts

qui le cachent

sinon tu ne verrais rien

aveuglé

moraliste venu trop tard

comme tous les moralistes

qui se respectent

n'eût-il pas valu mieux

que tu ne visses rien ?

délicatement peut-être

un rayon ---

sans action

nuages

sur la sainte victoire

terre rouge

sous les pieds

humide

nuages

sur la sainte victoire

aveugles

comme au premier soir

nous aussi

oublions

que nous sommes venus ici

seuls

dans l'espoir de ne faire

aucun bruit

et des villes

tout autour

pullulant

oiseaux de nuit

hululant

on a bétonné

le motif

au nom du nom

impropre

le nom de l'autre

immense

sans visage propre

armée d'âmes grises

sans vocation

et qui hantent

la succession

des chutes sans fin

que paraît-il

on nomme

histoire

qu'importe or

ce que l'on nomme ainsi

si ce n'est jamais de ton corps

qu'il s'agit

si ce n'est jamais ton corps

qui agit

mais celui obèse

d'une espèce qui s'agite ?

qu'importe or

si ce n'est pas de ton corps

qu'il s'agit

mais de ton cadavre

assagi ?

des cieux

séditieux

délicieux

à la marge

mon amour

le bas-côté du chemin de croix

à la marge

mon amour

les aléas

croisée des chemins de faire

à la marge

mon amour

c'est un service que l'on se rend

s'éloigner comme tout étant

et

tirer sans ailes les avantages

du néant

heures de sommeil

qui disparaissent dans la nuit

enfouies

sous les épaisseurs de l'inquiétude

infanterie des enfants

trous dans l'histoire percés

introuvables au matin

est-ce que j'ai rêvé

qui sait ?

phases blanches dans le noir

écoute les phrases

laisse l'enfant parler

après lui avoir récité le poème

elle dit de paul verlaine

mais ne s'endort pas

alors veille jusqu'à ne sais quand

est-ce que j'ai rêvé

qui sait ?

de la fin de l'adolescence

un peu avant l'âge adulte

insultes ivresse fêtes

folles

ne jamais vieillir

ou au moins

ne pas devenir vieux

des années plus tard

c'est le même monde en pire

qu'il aurait fallu changer

si tu l'as rêvé

dis-toi bien que

tu ne l'as pas oublié

la marmaille

sous la lumière crue

n'a pas l'air moins nue

mais plus encore

sans plus rien qui dissimule

sans plus rien qui simule

cris d'enfants sales

la rose électrique

au loin

ne me dit rien

elle tourne

elle tourne

et

personne n'en revient

je regarde parce que c'est intéressant

dit l'enfant

et

c'est vrai que

de loin

on pourrait voir la terre

mais non

ce n'est qu'un morceau

mal fichu

qui du néant

a chu

marionnettes mondaines

petits pas dans le vide

plus rien qui ne tienne

pas même à un fil

les pantins ont quelque chose d'excessif

ils brûlent

hommes de paille

ils brûlent

et

tout le monde

fout le camp

musique difficile

pas inaudible

qui réclamerait de nous

simplement que nous soyons soyeux

soyons doux

oreilles ouvertes comme toujours

à tous les vents

grand transit sans lequel rien ne subsiste

soyons doux

dit la musique difficile ---

soyons nous

à force de ne plus croire en rien

si seulement il s'était passé

quelque chose

oui mais quoi ?

rien

c'est toujours ici qu'on revient

sur la rive du néant

si seulement il s'était passé

quelque chose

mais non

rien

y a-t-il encore des questions qui se posent ?

je voudrais les prendre

à pleines mains

mais il n'y a que des notes

qui coulent

entre mes doigts ---

tout ce qui m'échappe

heureusement

que tout m'échappe

que je n'appréhende plus

rien

musique difficile

contre toutes les rengaines faciles ---

à force de ne plus croire

en rien

de l'espoir en moins

tout contre l'espace ---

il se rétracte ---

mondes étroits

où s'abolissent les langues

les croyances en la distance

canettes de boissons sucrées jetées dans le fleuve qui vient se jeter dans la mer

qui s'en va jeter ailleurs

les derniers détritus de l'étant

assis face à la mer

tu peux toujours rêver

mais

déjà le kitsch s'est converti

en son négatif --- maladif ---

trop de métaphore ---

trop de sucre ---

trop de graisse ---

écœurement

face à la mer

dans le dos

une image précise

de la mort

trames fleurs et passe

parfum de l'épaisseur

le jaune n'est pas un motif

encore moins une couleur

effet du narcotique

sur l'espace tout autour de soi

il effleure le temps qui passe

plane --- dit la surface

l'épaisseur du parfum

les brins bientôt

tomberont

un à un

regard hautain

quasi vide

de l'enfant aux chansons

ecchymoses ensuite

dit la peau

bleu tirant sur le jaune

tirant un peu trop

et pousse aussi vers le vide

une chute un choc

sang retenu

ou infiltré qui sait ?

tout finit par s'écouler

s'écrouler

c'est le sens de l'existence

le chas à l'autre bout de quoi

de ton corps transpercé

quelque chose

coule

chaos

toujours la question de l'espoir

qui vient et se pose

avec lenteur

sans l'appui de la main

un pied devant l'autre

après tout

y eut-il jamais

meilleure façon d'échouer ?

pas de sens

de l'effort

apprends à aimer tout le monde

et puis aussi le monde entier

toute une vie passée

à lutter contre soi-même

dans l'attente du mouvement

contraire

qu'auras-tu accompli ?

faire l'économie

hagiographie du signe moins ---

retrancher soustraire

quel jour ?

bien assez tôt sans toi

peux-tu faire l'économie de toi ?

te retrancher à l'intérieur d'une coque creuse d'un espace nul coquille cérébrale et attendre là quelque chose qui est déjà venu ne viendra plus ---

derrière la somme des apparences

or

même sur cette idée

pèse le doute

jamais trop de légèreté

finir

l'impossible

tout n'est-il pas

en effet

achevé

avant même

que d'être

commencé ?

une image de l'éternité

molle

nulle

au sens de triste existence

car en fait c'est d'être trop lourde

chargée

étouffante

qu'elle confine à l'ennui ---

frontière toujours franchie

et

repoussée par suite

toujours plus loin

mais

en vain

à qui la faute

à qui le doute ?

effondrement à la fin de la ligne

catastrophe

c'est-à-dire

non pour que tout s'achève encore

plus triste

non

rien que tout le contraire

que quelque chose ait lieu

ne serait-ce que ceci

disparaître

figues dans l'assiette

sèches

elles creusent au loin

le bleu de la mer

ou le gris du ciel

couleurs inverses

on peut toujours chercher à les fixer

rien n'est stable que le vent

il perturbe tout

le temps

met de l'ordre dans le chaos

météorologique où

paraît-il

il nous a été donné de naître

cadeau étrange n'est-ce pas ?

comme ces fruits qui fument

l'espace entre la brume et le vivant

l'écume et le néant

d'un côté l'eau

et de l'autre la pierre

la peau

précaire

où sommes-nous sinon cernés ?

membres épars

pâles quoiqu'écarlates

cinématique du climat

quand tôt le matin

un peu à peine après l'aube

le soleil touche

orange l'île

archipel agrume

meilleure façon de dire je crois le paysage que paysage

les yeux s'ouvrent alors

le doigt se tend à l'intention de l'enfant

nymphe

regarde

lui dit-on

et c'est ainsi que le monde découvre le monde

depuis la nuit

qui le précède

adossé à la falaise

dans la transparence de l'air

hiver

attendre que le soleil t'avale

plus loin ---

je dirais sur la droite ---

plus loin

un vieil homme attend assis sur les marches d'un établissement de santé publique

sale

seul

régime azur dans la roche

romarin en fleur

petites mauves

la pierre est luisante glissante

« il ne faut pas venir ici quand il pleut »

dit la vieille quand je la croise

mais pas à mon intention

non

je ne suis pas ici pour parler

aujourd'hui le soleil aveugle

on voudrait se dissoudre

et puis je me dis

« on » je ne sais pas

mais

moi en tout cas

oui

est-ce que la lumière s'éteint

comme les corps disparaissent ?

est-ce que les yeux se ferment

comme les centres commerciaux ?

est-ce que toutes les nuits

sont les mêmes ?

est-ce que tu regardes le destin

comme un film à l'affiche ?

est-ce que quelque chose t'apparaît

au milieu de la nuit ?

tu dors

tu dors et puis

c'est l'éclaircie

une lumière à minuit

quelqu'un est rentré tard

ou alors c'est le mystère

qui se révèle enfin

et toi

tu n'as pas fermé les volets

les illuminations ont ceci de spécial

qu'elles ne brillent jamais

que d'un seul côté

depuis quand

tout est-il fini ?

la rumeur sourd depuis le ventre des siècles

agents muets cachés

sous les gargouilles

devise des peuples qui ont trop dit

ou pas assez

qui sait ?

qui sait quelque chose après tout

ou si tout est fini

qu'avons-nous appris ?

tout finit ---

qui ne voudrait le croire ?

la lumière devient noire

nulle drogue ne fait plus d'effet

avale les paroles comme tout le monde

son cachet

ordonnance de l'ordre du monde

tout est fini tu sais

à quoi cela te sert-il

la fatigue

que faire du sentiment d'exister

qu'exprimer quand plus personne n'écoute plus

personne

ne peut plus écouter ?

qu'exprimer ?

dis ---

de l'air

n'ouvre pas les bras

ne regarde pas en toi

ni en l'abîme

ne fixe pas un point absent

qui toujours fuit

te fuit

n'accueille pas le monde

n'écoute pas les histoires de renommées

longues litanies de la vie fantasmée

ne sois pas tout à ton affaire

ne cherche pas une raison de plus

une raison de moins

il n'y a de causes que négatives

guerres lasses qui se laissent vaincre

vagues sans appel des gouttes

et des gouttes

sans la moindre théorie

n'ouvre pas les bras

grand

si tu retranches tout

que reste-t-il ?

demande-le-toi

et oublie-le

laisse-le

vivre

ne choisis rien

il n'y a pas de destin

tout coule

ou bien rien

aime quelqu'un

dis-le-lui

aimons-nous nous adonner à la laideur

ou sommes-nous tels êtres

devenus insensibles ?

anesthésie internationale

expansion et caetera

à l'infini du périurbain

à la manière de païens

rites

bloc après bloc après bloc

on cimente l'univers

civilisation de l'inculture

inculture de la civilisation

pendant que

dans tes yeux

se méduse l'éternité

reflets du ciel

dans le goudron

tout le malheur du monde

prend forme dans une coulée de béton

et

tu te demandes :

qu'est-ce qu'exister ?

s'extasier

devenir faire

semblant

et l'amour

mourir

pourquoi est-ce que quelque chose aurait du sens et autre chose non ?

tu te concentres sur le soleil de l'hiver

si beau qu'il dure à peine

tout pourrait disparaître

penses-tu alors

cela ne ferait nulle différence

tu sens que ce n'est pas vrai

non

mais

ce n'est pas une question de vérité

non

mais

de lucidité

faut-il que tout devienne clair

si clair ?

qui suis-je pour oser m'exprimer ?

qui sommes-nous pour oser exister ?

le monde ---

le monde

que le mot à la bouche

avale-le

aspire-le

sens est l'esprit

avec qui faire quoi

sinon corps

tout commence par là

comme tout y finit

erreur

vouloir que tout soit

lourd de sens

qui ne veut

la mort de gutenberg ?

profusion obèse du sens

gras du signe

qui ne veut

supprimer au contraire ?

élimine

crie une voix quelque part

injonction illuminée

brasier de l'ordre

elle a des accents déchaînés

parfums de garrigue en flammes

odeurs âcres du consommé

incendies du saint esprit

vapeurs de sens qui flottent

encore

après la consomption de la montagne

qui les sent ---

toi ?

que fait un corps

qui dessine

pense pleure chante

aime

en attendant

la résurrection des cigales

que fait un corps

qui peint

observe et étreint

en attendant

l'intervention lointaine

que fait un corps

qui sent

inspire et imagine

envahit l'espace

en attendant

la destruction prochaine

de l'espace entre les âges

de la forme même du paysage ?

que fait un corps ?

dis-je

et c'est une question

portant peut-être

sur la nature du feu

monde gris

percé par endroits de vie

tels trous

béances dans le réel

qui soudain s'anéantit

devant lui

ce rien ou quasi

soleil que les nuages détourent

ligne qui plonge dans la mer

et multiplie

parfois c'est vrai on a l'impression qu'il fait jour et on croit pouvoir s'accrocher à cette idée

cette apparence

comme si elle possédait

quelque durée

sauf qu'il n'en est rien

la masse qui nous surplombe est mouvante

la ligne illuminée

engloutie bientôt

antichambre de nulle part

s'avancer à l'aveugle sans savoir où

à l'évidence

à la télévision

alors

un type célèbre raconte que comme tous les gens de sa génération enfant il passait huit à neuf heures par jour en moyenne devant la télévision

éclaircie négative

dans le noir absolu de toute époque

le réel ignorant la merci

la grâce est inefficace

qui arrachera le voile gris

opaque

sur le paysage

au-delà d'une première ligne

que tu dessines

voiles

que cherches-tu à voir ?

le miroir fait toujours son effet

mais n'est jamais qu'une image

floue collée

à l'autre d'elle-même

toi ou bien personne

si tu plonges la main dedans

en ressortent des cristaux de sel

à même la peau greffés

j'attends un instant

bâille

narcisse est une fleur

tu sais

suave la vérité

dans son drapé

sauve

peut-être pas

légère en tout cas

pas un spectre

qui hante les zones désertées

de l'esprit

à peine une âme

une plume plutôt

comme les oiseaux qui viennent faire leur nid sur le toit au-dessus

à l'abri des tuiles

en perdent chaque fois

un coup de vent soudain

et c'est toute l'organisation

qui vole en éclats

qu'auras-tu fait des noms ?

déraison sens et négation

étonnements rouges

à la fin de la journée

quand on montre du doigt

ce qui déjà n'est plus là

qu'y avait-il à voir

de toute façon ?

aussi écoute

mauvaises chansons hymnes populaires

professions du néant qui s'érigent

phallus maladroits

en monuments

les couleurs ont passé

restent nos larmes

fades

les modes se succèdent

restons-nous seulement ?

à la fin

de ta ploraison

te sens-tu soulagé au moins

affiné par le vide

le vin

mince

ou bien n'est-ce qu'une façon innée

de passer le temps ?

je regarde le soleil se lever

un oiseau ---

le énième traverse le champ de vision

ce n'est pas encore le ciel

dis

ou déjà ?

musique difficile ---

dans l'instant où tout disparaît

ne restent que couleurs

comme les simples

au bord de la route

que personne n'emprunte

où ne restent plus qu'empreintes

faux mots comme suv

d'ailleurs elle n'existe plus

la route

l'état

disent les simples que nous sommes

pareils à des plantes qui parlons

pour le plaisir de parler

l'état

l'aura privatisée

et la boucle de se boucler alors

mais aura-t-elle seulement jamais été ouverte ?

musique difficile

tu dis

est-ce un rebours

ou un détour ?

quelque part il y a toujours quelqu'un qui hurle dans le silence

et une oreille qui l'écoute

quand même elle ne l'entendrait pas

ce chant absurde et pur

qui dissocie le monde depuis la nuit des temps

rupture dans le continu

failles dans la nudité

pas de promesse d'éternité

qu'en ferions-nous

nous

face à l'infini ?

tout est déjà si long

tu sais

un chant

c'est bien assez

comme la musique difficile

que tu t'efforces d'inscrire

définitions malpolies

sur les murs des cités ---

ils se sont déjà effondrés

et l'on n'entend plus que l'écho

lointain un peu las

de cet immense fracas

musique difficile

répète-le

slogan facile ou quasi

qui a demandé du sens jadis

et qui lui en a servi

à quoi bon vivre ainsi

dans l'attente de quelque chose qui ne viendra pas ou qui est déjà venu

et dont personne n'a voulu ?

musique difficile

que voulais-je dire par là déjà ?

voulais-je faire diversion

ou bien singer l'invention ?

faire semblant

n'était-ce pas avec cela qu'il fallait rompre ?

se séparer

indépendance de l'individu

tout nu

devant la caméra

qu'est-ce que je peux faire

se demande-t-il

qu'est-ce que je peux faire

apeuré comme un ver

qu'est-ce que je peux faire

sinon exister ?

musique difficile

je ne sais si je l'ai déjà dit

les saisons passent

vois-tu

et moi j'oublie

on croit quoi ---

que chanter suffit ?

le mensonge est sur toutes les lèvres

mais le vent souffle

et les idées disparaissent

moi parfois c'est vrai

je cours après

sauf que je sais

que toutes les courses sont vaines

à commencer par le soleil

hiver comme été

champion hypocrite des mères au foyer

qui sont aussi des hommes

qui passent l'aspirateur le balai

dans un moment d'égarement

tout le monde perd le fil de sa journée

et regarde au lointain

quelque forme qui se pourrait dessiner

horizon ciel soleil plaine parking ou solitude

tout le monde cherche

un endroit où habiter

ici ailleurs

qu'importe

dès le moment

que c'est

chez soi

je regarde le temps qui passe

le temps qu'il fait

une fois de plus

une fois de moins

quelle différence est-ce que ça fait ?

bientôt tout reviendra au même

même ce qui est différent

en attendant

passons de la musique difficile

jusqu'à

épuisement

que dire du bleu insensible

à notre misère ?

le ciel ne nous regarde pas

ni la mer

plaine

fais le compte des bateaux qu'elle porte

v=d/t

et reviens à

zéro

sommes-nous des îles

ou bien des déserts dans les villes ?

on méprise les concepts

et oublie ce faisant que

c'est

la mort qui a le plus peur

de la mort

par l'ouverture

couleurs primaires

plates opaques

tandis que rien ne s'oppose à la transparence

du visible

quelque chose sans forme

quelque chose pour quoi

nulle forme n'est nécessaire

regarde la surface

longuement ---

est-ce que tu crois que je peux devenir

cette surface ?

nous demanderions-nous

si nous étions encor

enfants ---

quatre crayons dans une

trousse improvisée

trois plus un

noir

cent fois cyan

cependant que

la mer inviolée

me nargue ---

à vol d'oiseau

penser à déconfiner les esprits

(autrement difficile

que confiner les corps)

miroloï de la vierge

et durant

choses qui passent dans l'esprit

tant à perdre

plus jamais perdu

omphale du monde

d'où sort-on

quand on ne sait où aller ?

même masqués

les visages sont les mêmes

sous les traits invisibles

de la pensée magique

action à distance

mystères de la science

occulte

comme le cul d'un éléphant

mort évidemment

où va-t-on

quand on n'a pas le droit d'y aller ?

tout le monde

à la même enseigne

prison pour l'éternité

ou comment accomplir enfin

le vieux rêve de l'égalité

à la fin ---

dis-le puisque c'est ce que tu penses ---

à la fin

il n'en restera plus aucun

de la grotte à la grotte

la boucle est bouclée

on s'inspecte

on s'ausculte

que croit-on trouver ?

on n'arrête pas

le progrès

non

on tourne en rond

de caverne en caverne

ne jamais rien changer

que la masse superficielle

boursouflée

qui nous tient lieu

de pensée

au lieu que

si l'on voulait s'y attarder

du rocher

où nous sommes entés

on pourrait tirer

quelque vérité

ou moyen plus radical

de mettre un terme à

nos souffrances barbares

primitives

de ne pas changer

à la vérité quelquefois ---

je dois à la vérité de la dire ---

à la vérité parfois

il manque un pied

(à la vie aussi)

la bonne mère

elle au moins

brille toujours

illumine

sans ciller

mer blanche

par moments

surface plane

par endroits

quand elle ne reflète plus rien

parce que plus rien ne s'y passe

parce que plus rien n'y passe

ni personne

la mer atteint à une vérité plus grande

plus dure

cruelle sans doute

le calme de l'existence surpassant en tout

les grands fracas de l'histoire

petites rides insensibles remous

clapotis inanes

ce n'est pas par nous

in fine

qu'elle s'achève

mais par la mer

infinie

ce qui fait jouir si fort

le clampin

pensé-je alors

c'est que jamais

il n'a été si facile

de faire le bien

à présent qu'il se réalise

de lui-même

en ne faisant rien

l'enjambement

dit pierre parlant

au téléphone

comment dessiner une île

les yeux fermés ? ---

d'abord

je laisse le bruit envahir le silence

tout ce qu'on ne contrôle pas

maîtrise pas

voudrait dominer

mais ne peut pas

tout cela je le laisse là où

c'est

et

une fois fermés

les yeux

si j'ai un peu de chance

des taches apparaissent

images rémanentes

souvenirs de quand j'avais les yeux ouverts

je ne peux pas les saisir

non mais je peux croire en elles

me dire qu'elles font partie de mon champ de vision

nouveau

cet espace opaque et pas si sombre qu'on ne le croit qui vient de s'ouvrir devant moi

si je ne les vois pas

à l'inverse ces taches

je peux les inventer

et peut-être est-ce mieux ainsi

peut-être est-ce mieux de ne rien voir

de ne rien croire voir

de ne rien faire comme on l'avait supposé

de toujours compter sur l'accident

le tsunami

la submersion du sens dans quelque forme qui ne lui appartient pas

à laquelle il ne peut pas appartenir

que je voie des taches ou que je n'en voie pas

qu'est-ce que cela change après tout ?

je sens déjà le poids de mes paupières qui s'enfoncent dans mon crâne

je sens mon corps s'involuer tout entier

former spirale comme dédale

se compliquer vers le dedans

moins le repli qu'un dépli vers l'infiniment petit

on s'enfonce moins qu'on ne se déploie autrement

j'ouvre alors les yeux

je sais ---

que ce soit ici ou là ---

je sais bien qu'une île est impossible

il y a toujours un archipel

quand même il n'y aurait pas continent

que des segments

pendant sans rien qui les relie

nous ne sommes pas si perdus qu'il nous le semble

ou morts c'est idem

non

nous pouvons encore respirer

les nuages prennent la forme du monde qu'ils surplombent

passage obligé

ou bien disparaissent

victimes de l'éternel oubli

parfois on montre quelque chose du doigt

et on dit

c'est un avion

mais on ne sait pas si c'est vrai

ou s'il va s'écraser

perspective

sur le monde

dans le ciel

on ne voyait plus ces

traînées

d'ailleurs

auxquelles on avait fini par s'habituer

mais qui ne faisaient signe de rien

sinon vers notre capacité à vivre

sans vraiment savoir pourquoi

quelque chose comme l'odeur lourde et subtile des pages moisies quand à côté de moi je hume les œuvres complètes d'élie faure

tu aimerais croire en quelque chose

mais pourquoi ?

qui se soucie des poupées ?

au deuxième jour de la quarantaine

barbie

a déjà l'air d'une actrice droguée

sainte paumée

sur le retour ---

allons-nous tous finir ainsi

morts ou bien moisis

cadavres ambulants sans plus rien

dedans

que des souvenirs d'avant

quand la vie valait la peine d'être vécue

ou quand nous étions assez insensés de fait

pour croire que c'était

vrai ?

le vrai mon gars c'est toujours la mort

quand même certains diraient qu'on ne la vit pas

l'homme ---

un peu trop vieux peut-être ---

je m'en souviens

disait regarde

mais plus personne n'avait envie de voir ce qu'il montrait

on pensait

à juste titre c'est probable

que c'était devenu invisible

ou aveugle pour notre vue d'ensemble

si l'horizon dépend toujours du point de vue où l'on se place

à quoi sert-il d'en changer ?

mais ce n'était pas cela qu'on se demandait

non

mais plutôt

de quoi est-ce qu'il me parle celui-là ? ---

et ça

personne ne le savait

faut-il toujours qu'il y ait une chute ---

quoi d'autre

quand tes poèmes portent

sur des avions ?

bleu

ou de toute autre couleur

le monde ne disparaît pas

tu sais

de la différence entre apparaître et son contraire ---

présence plus ou moins insistante

que ferais-je s'il fallait que

je me taise ?

imagine la quarantaine de

l'écriture

mais qui ?

qui t'empêchera jamais d'écrire ?

tout le monde

tout le monde ?

tout le monde

n'en est-il pas toujours allé ainsi ?

et puis le silence se fait

c'est terrifiant

personne n'y était préparé

personne ne se prépare jamais à mourir

pas même pline l'ancien

les philosophes en vérité

les philosophes sont des inconscients

ou bien

des poules mouillées

(bipèdes sans plumes)

expérience esthétique immédiate

à moins que

sans doute

plus rien ne veuille plus rien dire du tout

l'écran plat

et sans péché

sur lequel

tous

nous projetons

l'ultime de nos désirs

un corps un poème un triomphe

un ordre une victoire une rage

une vache un sage

une injonction une chanson

une maxime

plus ou moins débile

qui le regarde ?

sinon celui qui n'y verra rien

et moi

et mes yeux qui saignent

à moi

et sans le son même

oreille sans ouïe

je ne cesse de regarder

medusa fascista

espèce universelle

mais cool

le visage souriant

d'un monstre humaniste

il est là

et toi

invisible crapule

suce son cadavre insipide

bientôt il ne restera plus rien

ni de lui ni de toi

mais de qui on se souviendra ?

qui sommes-nous pour désirer quelque chose qui ne viendra pas ?

qui sommes-nous pour désirer l'avenir ?

les jours passent

tu sais

et le trépas ou la vie

sont-ils tant les facteurs de l'attitude ou de la survie ?

je regarde ce que j'écris et je me dis

le pire, c'est quoi

écrire ou bien souffrir de myopie ?

j'ai posé mes lunettes

là

juste à côté

si le monde s'effondrait demain

mais non ce n'est pas la bonne façon de poser le problème

tu peux accepter que le monde périsse

s'effondre ---

la fin du monde a quelque chose d'acceptable

on peut en faire un roman

mais ce n'est pas de cela qu'il s'agit

il s'agit de ta mort

à toi

cette chose privée dont tu essaies de te distancer

histoire de subsister quelques minutes de plus

mais à quoi bon ?

ne sommes-nous pas déjà condamnés ?

vas-tu donc désormais t'exprimer uniquement en ces vers étranges que tu prétends --- paraît-il --- braqués sur la réalité ?

où sont nos paroles passées ?

alexandrins et bouts rimés

--- chut ! ---

les vacances sont d'ores et déjà annulées

pour ce qu'il nous reste à vivre

de l'éternité

tu imagines un corps et il n'y a rien dessous le nom que tu lui donnes

certains disent que c'est la rime

d'autres la pensée

combien de temps peux-tu attendre

ainsi

c'est-à-dire insensé ?

mais personne ne parle

tu sais

les gens ne font que jacasser ---

il y avait une dame brune avant

me dis-je soudain

mais que je m'en souvienne ou pas

que tout le monde l'ait oubliée

quelle différence est-ce que ça fait ?

combien de poèmes faut-il écrire

avant que l'un d'eux soit lisible ?

tout le monde attend quelque chose

quelque chose qui ne vient pas

quelque chose qui ne viendra jamais

qui sait

n'est-ce pas là tout le charme de la vie ?

quelque chose se passe mais tu ne sais pas quoi

et quand même tu saurais

tu ne saurais pas ce que cela fait

je regarde les gens

les choses promises et puis les autres dues peut-être

qui sait

c'est vrai peut-être que tout le monde va crever

mais est-ce une raison d'hésiter ?

tout est fonction d'une chose

simplifier

j'ai envie d'écrire 500000 poèmes

quitte à ce qu'ils soient tous mauvais

je n'écris pas pour ---

je n'écris pas pour quoi ?

qui le sait ?

tout le monde dort ou tout le monde est mort

n'est-ce pas la même chose ?

avant

quelqu'un aurait fait des claquettes

ou des autodafés

aujourd'hui

il ne te reste plus que tes yeux crevés

ou ces visions débiles dont toi-même

c'est la vérité

si on te le demandait

tu ne saurais pas trop quoi faire

un vers n'est pas un retrait

non

je le crois

un vers est la négation du retrait

la négation de la chose

celle-là même que tu découvres

chez philippe jaccottet

avec ses alexandrins et ses bouts rimés

et toi qui découvres l'existence chaque jour que dieu fait

toi dis-je ou bien dieu ---

est-ce quelqu'un de différent ? ---

toi comment peux-tu vivre ainsi

comment peux-tu soupçonner ce qu'il se passe en ce bas monde ?

de toute façon

tu le sais

tout ---

je dis bien tout ---

tout ce que tu fais sera toujours

humilié

dégradé

armée de ricaneurs

en embuscade

ainsi il sera fait

parce que tu ne seras pas comme tout le monde

tu n'es pas comme tout le monde

et tu ne le seras jamais

on ne changera pas le monde

dit

celui que le monde a déjà changé

je regarde la mer

le calme plat a des allures d'enfers qui ne disent pas leur nom

unique

on se surprend à regretter les haines

ordinaires et

l'indifférence

au lieu des regards honnêtes

que les gens se jettent

comme si la méfiance

était un vaccin ---

le virus est en chacun de nous

c'est l'histoire de l'humanité

et pourtant nous restons enfermés

petits êtres obéissants

que la peur rend d'autant

plus impuissants

après l'infection

l'extinction

on se jette des regards

comme d'aucuns des pierres ---

on a les intifadas qu'on peut

où passe la limite

à présent qu'on la trace

de force

où passe la limite

entre ici et ailleurs ?

si l'on sent bien le proche

on découvre que le lointain

est à portée de main

tout ---

je dis bien tout --- est une question

d'état d'esprit

(cosa mentale)

et partout c'est la guerre

500000 poèmes

500000 morts

500000 raisons de vivre

500000 raisons de laisser tomber

abandonner

et partout c'est la guerre

toutes les histoires

se résument à la fin

en un bouquet ---

et il n'y aura jamais eu d'autre histoire

que cette fin

celui qui traverse

celui qui reste

hortensias improvisés

de qui

ne sait

dessiner

hier

ou avant-hier

je ne sais plus

je crois que c'était avant-hier

j'ai vu un type qui avait arrêté sa voiture

sur le bas-côté de la route

et était occupé à décharger un caddie plein

dans le coffre vide de sa voiture

ensuite

cela n'a pas duré longtemps

je l'ai observé faire

ensuite

il a refermé le coffre de sa voiture

il est monté dans la voiture

il a démarré sa voiture

et il est parti

laissant là

sans plus s'en préoccuper

le caddie

je ne suis pas resté interloqué

non

en fait

c'est un peu bizarre de dire ça comme ça mais c'est vrai

en fait

je m'attendais à ce qu'il s'en aille comme ça

laissant tout derrière lui ce dont il n'avait plus l'utilité

et je me suis demandé

quand l'humanité ne nous surprend plus

ni en bien ni en mal

n'est-il pas trop tard pour s'inquiéter ?

le monde s'arrête

à la bordure du corps

ne s'étend pas plus loin

n'est pas plus vaste

que ces quelques mètres carrés-

là

petit cercle égocentrique des perceptions

petit cercle inconscient

que personne ne délimite

avec lequel tout le monde naît

et meurt

et partout c'est la guerre

(refrain)

et partout c'est la guerre

la sécheresse est une question

de toucher

on s'embrasse avec des pieux

on s'embrase pour des non-lieux

on avoue tout au premier venu

et puis on se terre la nuit

venue

et jusque dans le jour ---

l'humanité est une espèce

en constante voie d'apparition

jusqu'où s'élever

ou descendre ?

un instant n'est pas

un point dans le devenir

quand l'atmosphère est bleue

question de lumière jaune

peut-être

tu tournes la tête de l'autre côté

et tout est effacé

combien de temps s'est écoulé ?

est-ce la bonne question à poser ?

nous vivons plongés en nous-mêmes

là où seuls

des abîmes de perplexité

seraient susceptibles de nous sauver

ou sinon

d'aspirer à autre chose

que la condition où nous sommes nés

quelque chose comme l'air

légère

pure comme un jour chômé

l'air désert

des heures

qu'on a pas encore eu le temps

de désherber

peut-être ne suis-je

rien d'autre qu'un point rouge

tache de sang ou de vin

maculage

on tend une toile

ce qu'on appelle un écran

et dessus

on regarde le temps se décrire

au rythme d'une écriture

organique

pas mécanique

forme de l'apprêt

toujours prendre soin au temps d'arrêt

le suspendu qui scande la série de nos oscillations ---

tout ce qui vibre est vivant

ô profonde mer

souffre

avale nos festins

de morts fiers

destins de celles et de ceux

gigantesque assemblée

qui te tournent le dos

jusque durant le bain

l'été

ô profonde mer

souffre

pour nous

et nos récits

dont le sens s'est effrité

miettes de ruines

insignifiantes cités

à quoi donc nous consacrer

à la vaine archéologie

à la mythologie

ou bien

à une espèce de futur

logis de qui n'a pas encore de lieu pour

être

sinon

ici

dans les signes insensés

que nous nous efforçons

de tracer ?

ô profonde mer

souffre

et embrasse-moi

profonde mer

prends soin de moi

montre-moi les reflets de qui n'existe pas

les images déformées de moi

monstrueux avenirs

algues majeures

dont nous tapisserons les continents

partout l'archipel

et pullulation

partout la signification

il faut du temps pour détruire

achever de médire

ô profonde mer

fais-moi don du silence

la science du mutisme

elle mute

plus nulle parole de vérité

autre chose

qu'on ne sait pas encore inventer

ô profonde mer

souffre

souffre pour moi !

les pensées apprises par cœur se déploient d'elles-mêmes

roues qui tournent

à vide

s'enroulent

on est heureux de pouvoir reprendre en chœur

le psaume de l'identité

tous unis

il n'y aura plus de faille

plus le moindre interstice

d'air libre

zéro espace

zéro distance

quand tout le monde pense la même chose

zéro égale un

un égale zéro

n'est-ce pas ainsi que tout

s'achèvera

un jour ---

à la ronde ?

essaie de ne pas

penser ta pensée

au fond du champ

vision de la mer bleue

écran de fumée

ciel voilé

jaune pâle du ciel

on entend des bruits

si distants

qu'on dirait

étouffés

nous

ça va

nous savons toujours respirer

au loin

un homme hurle

rentrez chez vous

deux fois de suite

et puis plus rien

l'existence est

une suite d'épiphanies

négatives

au cours de laquelle

peut-être

on parvient à trouver

un moyen ou un autre

d'être en accord

avec l'univers

et partout

c'est la guerre

heure de promenade

réglementaire

air saturé de pollens

tombés du ciel

ou de l'arbre au-dessus

qui sait ?

petites bombes inaperçues

dans le jardin

où

des enfants plus tout jeunes

et gros

et laids

jouent à se désennuyer

tout ce qu'ils ont jamais appris

alors que c'est tout le contraire

qu'il faudrait enseigner

non ?

à aimer l'ennui

où se puise

la passion de l'utopie ---

je regarde autour de moi

et ensuite

au loin par la fenêtre ouverte

sur l'air frais d'un début de printemps

paresseux

pas d'espoir à l'horizon

me dis-je

y en eut-il jamais ?

me demande une voix

ironique

que je ne connais pas

nous errons

c'est étrange

nous errons

sans même nous mouvoir

zones immobiles

et géolocalisées

pourtant

où irons-nous

traîner nos mélancolies

nos élégances vices

si partout désormais

le grand œil nous précède ?

partout c'est ici

plus nul déplacement

à quoi bon

se demande-t-on

sortir de son appartement ?

le grand œil est aveugle

c'est vrai

mais il faut pour fonder la force

faire accroire une tout autre vérité

confiné au sublime

c=dπ

pas une formule magique

un rien

l'égal du dépit

circonférence de l'être

là

périmètre à parcourir

toute l'étendue du monde

se résume à ce

là

tourne tourne et tourne

et rayonne

tant que briller se peut

encore un peu

façon de vivre

de disparaître

de renaître

au-dessus dans la demeure limpide

olympienne

aimerait-on pouvoir dire

mais cela n'a plus de sens

pan immense

de ciel bleu

qui tire

son impassibilité

du vent qui le bouscule

nous sommes seuls de toute façon

conscients

quand même fous

à le cerner du regard

de notre impossibilité

une sphère finie

les sirènes hurlent

un véhicule de police

descend l'avenue

à tombeau ouvert

le temps à peine de se retourner

on ne voit rien qu'une inscription

en lettres capitales

bleues et disproportionnées

RESTEZ CHEZ VOUS !

manière de dire probable

pour qui sait interpréter

les signes les songes les oracles

qu'ulysse était un survivant

et que notre odyssée

à nous

désormais

consiste mais en rond

à tourner

géométrie de la terre

où partout

c'est la guerre

on aurait pu croire

c'est vrai

que quelque chose sortirait

de notre « conscience malheureuse »

que nous ne poufferions pas

comiques systématiques

enfants gâtés

dans notre certitude vareuse

de tout

du sentiment d'être même ---

mais qui ?

bien habillés

c'est vrai

que pourrait-il nous arriver ?

on aurait pu croire

mais rien

tout se boucle sur soi-même

on se demande ce qu'il s'est passé

on a l'air un peu hébété

c'est vrai

d'autant que c'est un mot compliqué

on se regarde dans le miroir

les cheveux ont blanchi

les traits sont tirés

on se découvre des cernes

l'enfant gît

sur le parquet

que s'est-il passé ?

oh rien

les chiens l'ont encore mangé

il y a quelques jours

je l'ai noté pour ne pas l'oublier ni douter que c'est vrai

un homme

a abattu ses deux chiens

à coups de fusil à pompe

il vivait avec eux

et sa mère et son fils

dans un appartement

du centre-ville

de toulouse

les chiens étaient sur le balcon

mais

comme on ne pouvait plus les sortir

ils aboyaient trop

à son goût

alors il est allé vers eux

et d'une balle ou deux

il a mis un terme

à cette nuisance infernale

bruit que fait la vie

quand elle se révèle animale

il les a tués

donc

et puis la police est venue le chercher

et il a été écroué ---

c'est vrai

quand on n'aime pas les bêtes

tu sais

on n'aime pas les gens

histoire naturelle

de notre perplexité

pas de différence palpable

je contemple l'air qui coule entre mes mains

pourquoi ne voit-on pas

en temps ordinaire

qu'il a la même couleur

que l'eau de mer

salée ?

je contemple l'air qui coule entre mes mains

pourra-t-on distinguer

le premier bain de l'année de l'apocalypse nucléaire

qui ne se produira jamais ?

assis sur un banc dans le jardin

j'entends un vieil homme

il parle trop fort

à un être non identifié

au téléphone

un autre plus jeune

donne des ordres à son fils

tente de lui faire exécuter quelque singerie

devant l'objectif

de son portable ---

rien ne ressemble tant à la vie

que la vie

ordinaire

belle

d'une douce matinée

de printemps

imagine

quelque chose de suspendu

temporaire

et mortel

le parfum

étouffant presque

insoutenable presque

des glycines

on finit toujours

par se trouver une mission

comme décrire

à mesure qu'il est détruit

et remplacé par un autre

dont on ignore tout

le monde dans lequel on vit

et qu'il faut dès lors

apprendre à décrire

bleu outretombe

la couleur est réminiscence

ce qui sourd partout

sans même le sens

bleu outremonde

autant s'immoler dans l'azur

on fait des tragédies de draminets

et puis on se regarde

baba comme bébé

quand on découvre

nos vies insensées

tout le temps passé

fini et foutu

bleu outremesure

qui te brûle les yeux

décillés enfin

c'est tant mieux

tant pis pour la vue

l'œil de l'esprit

est un aède délirant

qui divague comme un revenant

bleu outrelangue

la couleur irise l'essence

taisons-nous

tout est nu

et dire que l'on pensait

apothéose du progrès

en avoir fini

avec l'ennui

nous sommes une maladie

grandiose

ça sent la mort pure

l'entêtant parfum de la pourriture

nous avons de petites armes qui nous défigurent

l'espace public est contaminé

interdiction de circuler

qu'il est faible le bras armé

de nos gestes barrières

ridicules prières

d'un peuple qui n'a plus de foi

se laisse tout entier dominé par la loi

quand on la croise

la vieille dame se fige

tire un bout d'étoffe autour de son cou

qu'elle remonte maladroite

sur sa bouche

sur son nez

pas la peine de les dénombrer

les cadavres sont là

partout et debout

je les vois déambuler

bonnes âmes en peine

dans la rue

il suffirait

pour qu'elles tombent

de leur souffler dessus

et que s'écroule

avec elles

le petit édicule

sans force ni ferveur ni vigueur

où se tapit la civilisation

exercice

banal et beau

du quotidien

l'enfant qui invente des jeux

paraît chaque jour

un peu plus grande

à nos yeux

au fond desquels

chaque jour aussi à notre tour

nous semblons un peu moins

vieux ---

la vie a des façons de passer

ainsi

une force latente

qui ne jaillit pas

mais coule puissante

et permanente

comment se fait-il alors

que nous soyons aveugles

à elle

et que seule l'exception

l'urgence la crise

bref

la peur de la mort

nous la montre

à l'occasion

fugitive

telle qu'elle est pourtant

tout le temps

dans l'exercice banal et beau du quotidien ?

imagine-toi

en ulysse à la dérive

ne comprenant plus rien

soudain

au fonctionnement de son gps

dans le lointain

cependant que l'enfant danse dans le jardin

un homme à la voix éraillée

et folle

pousse un cri

à force de vivre confinés

vous allez tous crever

quelqu'un s'est exprimé

certes

mais rien n'a été dérangé

une certaine idée

de la démocratie

confinés

à la folie

monde immonde

où il m'aura été

donné de naître

que faire de toi ?

là où je suis

un brin d'herbe

ou bien un gisant

qui sait ?

je regarde la masse du ciel

grande et continue

défiler

il y a aussi des gens masqués

et je ne sais plus

à dire vrai

que penser

si c'est bien ou si c'est mal

de se voiler la face

mais je ne suis qu'une feuille

moi

légère

je peux m'envoler

je peux disparaître

quelle différence cela fait

mais l'espèce

elle

sa lourdeur

l'immense procession des corps sacrifiés

ne faut-il pas qu'elle reste demeure

survive ?

moi je ne suis rien

qu'une brindille

tout ce qui m'importe

ce n'est pas cela

non

c'est autre chose

moi je ne suis rien

qu'une herbe folle

tout ce qui m'importe

c'est la vie

quelque part

les machines n'ont jamais cessé de fonctionner

quelque part c'est-à-dire

partout

c'est l'arrosage automatique

c'est l'éternel retour

à rebours

les beaux jours

un peu comme

des ruines

mais à l'envers

autoroutes saturées

où plus rien ne circule

que l'information

quelques cadavres

en transit de vie à trépas

qu'on dénombre aussitôt

un à un

étrange transhumance et macabre

mathématiques à mi-chemin

entre la statistique et la numérologie

point invisible de l'inflexion

autant consacrer sa vie

à l'exercice quant à lui statique

de la génuflexion

en attendant de foutre le camp

ventre à terre

mais dieu sait où ---

tout désir étant l'inassouvi ---

parce que partout oui partout

partout c'est la guerre

film nocturne

de l'enfant fantôme

réponse définitive

autant que faire se peut

à la question improbable de savoir

pourquoi il y a quelque chose

plutôt que rien

est-ce vrai que

comme il n'y a rien

nous cherchons quelque chose

ou que

comme il y a quelque chose

nous aspirons au rien ?

toutes ces questions

qui semblent te faire tourner en rondes et

circulaires réflexions

dis-toi bien que parfois

elles te font gagner du temps

cependant que tu demeures

là

perplexe

tu ne songes pas à aller voir

ailleurs

là où

de toute façon

tu n'as pas le droit d'aller

demeurer ---

telle est désormais ta condition

demeurée

ou comment se déplacer

est devenu

un crime contre l'humanité

je regarde le plafond

et me demande

le blanc

est-ce une couleur ou non ?

toutes ces choses qui pourtant

passent inaperçues la plupart du temps

(note pour ego : je n'ai pas réponse à tout parfois si j'écris quelque chose c'est simplement que je trouve que c'est beau je peux rationaliser mais cela n'a jamais de sens que limité j'écris et puis c'est tout qui de toute façon pourrait bien avoir envie de comprendre tout ce qu'il fait ?)

quinze secondes de vérité

conditions d'exercice

dans les jeux de l'enfant

qu'est-ce que la vie bonne ?

le bonne hauteur d'homme ?

haut comme trois pommes

ou comme trois crânes

comme dans un tableau de cézanne

je passe des heures à regarder le ciel

je pourrais passer des heures à regarder le ciel

bleu

et ne rien faire d'autre que respirer

inspirer de l'air chaque jour moins impur

par opposition au sang

oublier l'héroïsme sans intelligence

préférer quelque chose

dirais-je

comme un esprit

sans personne dedans

à quoi bon une ligne de plus

si l'existence demeure indigne

inchangée

sans traumatisme

enfermée dans sa tête

laquelle est close

toujours close ?

la condition des visibles

(tâche de ne pas l'oublier)

la condition des visibles

est de disparaître

un peu comme on dirait

la condition des nuisibles

est de persister

je crois que ce n'est pas vrai

qu'il y ait toujours

un mot une parole qui sauve

on balbutie quelques formules

encore est-on heureux

de savoir certes articuler

les autres ont la langue qui pend

dans le désir muet de l'autre

inconnu mais nommé

une ombre intangible

sur laquelle les questions d'existence

reposent

et même se fondent

on essaie d'enter sur des simulacres

les rivages imaginaires

et s'étonne ensuite de ne même plus finir

noyé

on se gave et crève à la fin écrasé

sous sa propre inutilité

mais qui ---

demande-le-toi ---

qui pourrait bien encore vouloir

de toi ?

hantises

dans le champ chromatique

quelle est la différence

entre une mouche

et une obsession

une langue morte

et une autre

dont le sens est obscur

pour d'intimes raisons

comme cette façon bien à elles

qu'ont les mouches

de toujours revenir tourner

au même endroit

on a beau les chasser

elles sont là ?

ce matin ---

peut-être ceci est-il lié à cela ---

j'ai tué le premier moustique de la saison

dérisoire hécatombe

bêtes

sans dieux à qui les immoler

en face l'île mobile

inquiète mon âme

l'immobile

décrit des parcours fantastiques

périples

que nulle machine ne retracera jamais

mais que l'œil nu lui

quand il sait oublier qu'il en est un

l'œil qui luit nu

parcourt à perte de vue

et découvre des tableaux

jamais achevés

couverts de peinture fraîche

outrés

aux traits légers

quinze secondes de vérité ---

combien de siècles de vanité ?

pas un nuage dans le ciel

où fixer l'attention

qui dérive dès lors

le sommeil devient

quoi sinon l'horizon ?

mais je ne ferme pas les yeux

non ce n'est pas vrai

du moins pas assez longtemps

pour m'enfoncer

quitter la veille

j'entends une musique

quelques notes précises

si je ne puis les dénombrer

elles raisonnent

et ce qui trouve là à s'exprimer

n'est ni un mirage

ni mon intériorité

(qui a eu l'idée

d'ailleurs

de cette vie intérieure

qui d'autre que celui

qui était privé de toute vie extérieure ?)

quelque chose de plus subtil

et de plus solide aussi

il y a une mélodie

dans les choses

qui ne vient pas des choses

mêmes

mais à elles en réponse

de plus loin et pourtant

dedans

et partout c'est la guerre

ou tout ce contenu

entêté

sur nous déversé

flux obstiné

dans l'optique

unique fixe

comme une idée

de nous empêcher de penser

la bêtise obscène

la bêtise obsède

qui en monte la scène

arrière-garde du désir

rien n'a de sens

que bien entendu

tout est question d'entendre

de malentendus

tout le monde s'accorde à bien entendre

et pourtant

ne comprend rien

absolument

où serais-je moi

si je le savais ?

ici sans doute

à faire avec soin

discipline aérienne

ce à quoi ---

pauvre fou ---

et conscient de soi qui plus est ---

chaque jour je me consacre :

artillerie légère contre la méscience

(on les voit déambuler dans les rues

âmes en peine de rien sinon d'elles-mêmes

on les voit arpenter les rues pour le nourrir

avec vissée au ventre la peur du mourir)

tout doit disparaître ---

c'est l'histoire des soldes

c'est l'histoire de l'humanité

regarde-la et

apprends à pleurer

un tour plus un tour

plus un tour plus un tour

et ainsi de suite à l'infini

ne feront jamais le tour du monde

la circonférence d'ailleurs

a-t-elle jamais marqué autre chose

que la fin des grandes découvertes ?

à quoi bon sortir d'ici

à présent que partout c'est nul

et nulle part chez soi ?

d'où le dépit

l'éternel ennui

(fixe le regard sur l'avenir)

l'humanité est une espèce

en voie de mobilisation

au garde-à-vous

sempiternel

en quelque sorte

dispars êtres

pas de blocs

compacts

mais des formes singulières

disparaître

je peux les voir flotter fuser

assemblée éparse

nuées d'insectes ou d'humanoïdes

vol d'oiseaux dans le ciel invisible

je guette quelque chose

mais qui peut encore dire quoi ?

le monde ressemble à une interminable fin de journée

sans coucher de soleil pour l'orner

faire beau

de toute façon il y a bien longtemps

qu'on a enterré la beauté

il y a encore des stèles

çà et là

mais non

le ciel ne nous est pas destiné ---

est-ce la raison qu'à moi

il semble si beau toutefois ?

nos désirs ?

taches dans le champ d'horizon

rien de plus

gouttes de sang dans l'océan

infini des plaisirs

chaque jour en appelle

rouge majuscule un

qui s'enroule

est-ce mon cou ou une liane ?

la roue tourne il paraît

mais on ne sait jamais

les contempteurs du spectacle

ne sont-ils pas devenus des bêtes

de foire ?

passions tristes

poissons chats

femmes oiseaux

harpyes

hommes fontaines

tiers mondaines

foules déchaînées

des rats

fauves boiteux

riches et malheureux

sauvages amputés

armées de léopards nains

et édentés

révolutionnaires contrariés

déprimés

à l'idée même d'échouer

héros sans exploits

citoyens sans droits

regards menteurs

de chiens battus

toute une humanité

monstres

dans une grotte

enfermés

il faut vivre dangereusement

dit la voix du mutant

mais que l'on ne compte pas

sur moi

pour prendre des risques ---

les océans de plaisir

sont des temples de lâcheté

personne ne s'y noie

on n'a guère plus que des bleus

à l'âme

sans savoir en vérité

ni pourquoi

ni ce qu'elle est

fantômes zélés

mal à l'aise les étants

dans le monde

préfabriqué

comme les maisons qu'ils habitent

et que l'architecte

grand

produit en série

semblable au semblable

n'est-ce pas ainsi

qu'on s'assemble ?

je fixe ce point rouge que

j'invente

il chante peut être jaune

ou bien ozone bleu

s'il change moi avec

sans chercher à être comme lui

je change aussi

essaie d'aller là

où l'on ne t'attend pas

puisque partout ailleurs

partout c'est la

guerre

il y a un homme

il tourne en rond

autour d'un arbre

tous les jours

je le vois

ou presque

il est là

et il tourne

il tourne autour de son arbre

il porte un masque

des gants une casquette

des lunettes de soleil

il est trop chaudement

vêtu pour la saison

il porte un sac sur son dos

et il tourne

il tourne autour de son arbre

il pourrait aller n'importe où

il pourrait aller n'importe où

dans le jardin

il pourrait aller n'importe où

dans un rayon de un kilomètre

mais non

il tourne

il tourne autour de son arbre

tous les jours

ou presque

il tourne

il tourne autour de son arbre

quand parvient-on

au bout des choses

au bout du compte

au bout du bout ?

il y a une lumière noire

dans l'habitacle

mais comment savoir

si c'est une erreur

ou bien la seule lueur qui soit

sombre pas malsaine

attirante au contraire

là où tout ce qui est

se perd aspiré ?

y a-t-il seulement un bout des choses

ou est-ce comme ce monde

une fois parvenu à sa fin

on découvre une ronde

boucle sans fin mais

sans début non plus

sans rien ?

non ce que je voudrais

ce n'est pas ce qui recommence

revient au même

coda et caetera

mais quelque chose qui puisse continuer

durer

et je sais bien oui je sais bien

tout le tragique de l'existence

comme le silence et l'ineffable

vérité qui l'accompagne

(fable)

fit-il en balayant cette histoire

d'un geste revers de la main

mais ce n'est pas ce que je cherche

moi

alors quoi ?

il se tut un long moment

et reprit

les aurores et les couchers de soleil

un parfum inattendu

la sensation du printemps la nuit

des voix inouïes ou familières et qu'on aime

quelqu'un qui ne comprend pas

et le dit

l'enfant qui se force à rire

la douceur de la folie au saut du lit

un croquis quelque chose d'entier

parvenir à la fin d'un carnet

imaginer le chemin parcouru

mais sans se retourner

une déesse callipyge en chair et en os

les amours que nous n'avons pas choisies

le souci de la précision

la perplexité passionnée

la lumière à l'ouvrir des volets

la justesse

l'acidité d'un fruit sucré

et tout

tout cela qui ne se résume pas

c'est tout ce qu'il nous reste

pour nous accrocher à l'univers

nous unir au monde

les crépuscules

rouge sang

quelques rayons de soleil

des oiseaux

la mer n'a jamais semblé si plate

vue de loin

on pourrait rêver

mais cela ne sert à rien

tout est là

présent

ici et maintenant

le ciel voilé

au-dessus de moi

et la loi morale

tout autour de moi

sans mœurs

de celles

c'est-à-dire

sans lesquelles on meurt

les visages masqués

de plus en plus près de moi

la vérité dévoilée

de plus en plus loin de moi

et sous tous les visages cette peur

civilisée

tellement civilisée

qui darde les volontés

les réduit

à l'absurde

épuisées

le ciel bleu tirant sur le blanc

une tache aveugle

dans une voûte immense infinie qui sait ?

de confusion

l'escargot de l'histoire

ignore où il va

noir blanc

il mélange

et tout disparaît

derrière le rideau de la pluie

brume

noir blanc

c'est étrange

étendue nimbée

on pourrait s'y perdre

gris

est-ce que je devine encore quelque forme

les yeux fermés

ou est-ce que j'imagine

des choses des animaux

des esprits

y a-t-il encore de la vie

ou bien est-ce qu'un dieu moqueur

que tout le monde a oublié

en profite pour se rire

de nos essences maladroites ?

la place du mort

c'est là ou le désert

se faire violer à l'arrière

d'un véhicule à l'arrêt

tout le monde a tort

de vouloir une nouvelle vie

d'espérer un nouveau monde

un nouvel ordre de nos désirs

nos regards s'achèvent sur le pas de nos portes

derrière lesquelles

monastère ou bien mouroir

nous nous replions cloîtrés

où les on-dit qu'on colporte

font des gens des cloportes

que pense 72% de l'humanité

ou 72% de moi-même sur n'importe quel sujet ?

il y a de la brume accrochée à la falaise

au sud

verte et calcaire

un coup d'œil à ces cieux

ne prouve rien du tout

sinon qu'il n'y a nul dilemme

nous vivons toutes nos vies à genoux

raptus perpétuel

selon comme on le regarde

le bateau semble posé sur la mer

chaque instant immobile

le mouvement étant ailleurs

sans savoir où il va je voudrais

monter à bord et naviguer

mais pourquoi ?

cela n'a pas d'importance

ni le fait que je ne le puisse

que je sois assigné à résidence

ici comme l'âme dans le corps

aurait dit socrate à phédon

non ce fait non plus n'en a pas

d'importance

je suis en exil chez moi

avec l'insupportable sentiment

de n'être même pas le premier à avoir

cette idée

ciel absolu

sans rien dessus

parfois un avion le traverse

une apparition

occasion de se faire léger

comme l'air qu'on oublie

de respirer

visage bleu

on dirait tuméfié

de l'intérieur

dehors semble plus que jamais

inaccessible

problème du monde extérieur

existe-t-il quelque chose d'autre

que moi

puis-je me fier à mes pensées

vu où elles me portent

à quelle extrémité lointaine

quand je suis là

comme dément

hors de mon âme pourtant

où autre aller

tout entier occupé à étouffer ?

emphase avec le réel

qu'on me montre une fin un début

un moment qui vaille la peine d'être vécu

archipel des gouttes de la pluie

sur la fenêtre

pas envie de chercher aujourd'hui

quelque chose à quoi m'accrocher

à quoi croire à quoi rêver

le simple fait d'être là

je ne sais pas pourquoi

possède une vertu cathartique

je n'ai pas besoin de justifier

ce qui vient d'être vécu

ni ce qui va l'être non plus

tout est une question de

mais je n'ai pas envie de finir cette phrase

je ne veux pas d'un tel désir

de généralité

peut-être que c'est vrai

peut-être que je ne sais pas

ce que je veux

mais est-ce si utile si nécessaire

qu'on l'imagine ?

je laisse les ressources de l'imagination

en paix

encore un coup d'œil à la fenêtre

et par-delà

les nuages s'étirent

la grisaille se dissipe

si l'on ne meurt

on finit toujours par y voir

un peu plus clair

un avion tourne dans le ciel et

moi

j'ai envie qu'il s'écrase

pas que cesse son bourdon sourd

non

qu'il brûle dans les flammes

de son propre accident

pas une destruction gratuite

non

une manière de respiration

oui

un climax civilisationnel

disons comme quand

les gens font la queue

pour aller à l'abattoir

brûle

brûle

brûle

au-delà du style

exercices de sublimation ---

il ne me reste plus beaucoup de temps

histoire de faire semblant

que la lumière existe

encore

comment fait-on pour exister ?

c'est-à-dire

comment fait-on pour ne pas exister ?

je contemple les zones creuses

les pulsions obèses

et me demande

qui restera là

comme moi

sans mot à dire

sans lieu où aller

méduse au carré

à faire le tour

haletant

de la nécessité

trop de pierres

pas assez de pierres

trop de poèmes

pas assez de poèmes

trop de langage

pas assez de langage

trop de bruit

pas assez de bruit

questions d'équilibres

à trouver

où tout faire

basculer

paix relative

dont on capte les signes

extérieurs

en faisant semblant

un peu

en oubliant

un peu

notre misère grande

en suivant du regard

par exemple

un oiseau dans le ciel

où va-t-il

cependant que je demeure là

enfermé

sous peine de me voir

emprisonné ?

je rêve encore parfois

et au réveil je me demande

qui était cet être

libre de ses mouvements

un fantôme probablement

un spectre insolent

qui s'est moqué de moi

toute paix ---

relative à la guerre ---

manières de défaire l'immonde

fabulation des faits

c'est vrai

qui ne voudrait

la tranquillité

l'apaisement

quelque chose comme

la paix de l'esprit

gelassenheit man you see ?

plutôt que ce souci

permanent de la vie

et si et si

simulacres qui sillonnent le ciel

humanité pareille

ferais-je œuvre de moraliste

non pire

démoraliste

quelque part

quelqu'un parle

et nul ne l'entend ---

une histoire de la vie

quelque part

quelqu'un fait semblant

et nous sommes tout ouïe ---

une autre histoire de la vie

reine de chine

qu'elle pleuve ou brille

d'où viennent les nénuphars

qui peuplent notre étang ?

étranges pays que nous ne visiterons jamais

au-delà de la mer de calcaire

nageoires nous menacent

traces du passé ou présages de sciences

nouvelles et inespérées qui peut-être

nous sauveraient

qu'ai-je fait

scène divine

pour pleurer ou briller ?

des taches sur le soleil

en vidant mon verre

un soir d'été

enivré

qui se souvient encore de quelque chose

quand les yeux étaient ouverts

et les persiennes mi-closes ?

trop de questions

pas assez de questions

nous avons bâti un mur

dans le jardin

pour nous protéger de l'air

c'est beau

si beau que dessus

nous pourrions faire de l'art

comme on en voit dehors

dans les rues

mais toutes les fresques sont télévisées

désormais

on a épuisé le talent

et ne sait plus comment ça se regarde

quand ce n'est pas de l'image en mouvement

c'est que nous aussi

nous restons immobiles

là derrière notre mur

qui devrait nous protéger de l'air

si seulement il ne nous fallait

pour notre malheur

continuer de respirer

et partout

c'est la guerre

peuples de faibles

couards

hypocrites bandemous

dictateurs impotents

éternels prolétaires

et leurs banderoles

auxquelles se pendre par le cou

art de la petite bourgeoisie

nulle façon de vivre

sinon médiocre

on se cache derrière sa race

son masque son petit doigt

et on espère que quelqu'un vienne

qui nous sauve

or le ciel est vide

on expire

nos énergumènes l'ont déserté

ou bien peuplé de vierges

à enculer outretombe

pour le bonheur de mâles décérébrés

dont on se demande comment

ils peuvent encore subsister

peuples d'animaux mal élevés

impropres même à l'abattage

gamins hirsutes qu'on surveille

des puces plein le corps

sans prendre la peine de les regarder

peuples issus du grand ratage

big bang farcesque pour qui n'a pas d'idées

on fait semblant de vous entendre

de vous aimer de vous comprendre

mais à la vérité nul ne l'ignore

depuis le début tout est foutu

chaque jour

j'écris ma fable

vin autrement

couleur de sang

ou transparent

variations avec la lumière

dedans à défaut de la vérité

moins de vanité

éclair dans l'esprit

pas dans le ciel

où le vent grand

fait voyager les pollens

dissémine la vie

vin vie viens

soleil vers la fin de la journée

plus qu'une éclaircie

bien plus

ébloui je lis les aventures

d'un homme qui voulait rentrer chez lui

hibernohellène écriture

fonction de son âge

la mer est couleur de sang

le vin est couleur de sens

je cherche la bonne image

de l'existence dans

cette transparence

je préfère regarder au-dessus

loin des terre-à-terre étants

parfois les marques sur mes bras

ressemblent à des nuages

quand j'ai passé trop de temps

appuyé de tout mon poids

je les regarde étonné

les découvre déportées

sur mon corps

importées d'outremer

quand on regarde le monde par en bas

il n'a pas l'air assez étrange

pas l'air assez fou

lors que nous devrions perdre

la tête le sens l'orientation

à mi-chemin entre ici et nulle part

hominidés captifs de la raison d'être

déraisons d'espérer

comment garder le sang froid ?

quand le vent souffle

yeux en larmes et autres

allergènes

dieu semble très gêné

quand de lui tous ne voient rien

que le trou du cul

scissions dans la soie

frissons sur le moi

tout le monde cherche

quelque chose à quoi se raccrocher

tout le monde cherche

un lit où s'étendre

et les fluides de se répandre

au bout d'un laps long

les yeux se décillent

on ajoute une cédille au boiteux

un liquide s'écoule

tout le monde cherche

une raison de continuer

que ce soit ma satanique science

ou ton angélique ignorance

il faudrait être fou ou surdieu

pour imaginer un jour s'arrêter

temps étrange

impossible de vivre

ou alors sentiment

d'exister indexé

sur cette croissance

inaltérable

et exponentielle de l'absurdité

partout des vanités têtes

masquées contre une mort fantôme

des mains inventent des gestes

inusables pour se protéger

mais les yeux sont trop faibles

ne voient rien ni ne détectent

les traces les formes de la menace

en tendant la main

je pourrais presque la plonger

dans l'eau salée et salvatrice

de la mer

mais

chaque jour qui passe

semble l'éloigner un peu plus

ce n'est pas vrai me dis-je

c'est le poisson inverse

oh oui je le sais

mais qu'y puis-je moi

et que puis-je faire de cette sensation

d'avoir cessé d'espérer ?

lumière chaude mortelle

quasi

elle croît

au loin les collines

sèches vertes

effet du soleil

couleur de la pierre

parlent du ciel

qu'elles pénètrent

d'une géométrie des couleurs

du visible derechef

de la pointe qui se détache

sommet

quelque chose est montré

un endroit peut-être

que je ne sais pas encore discerner

situer

mais dont le possible me hante

déjà

pas un mystère à percer

mieux

des propriétés à inventer

annuler l'ex

faire quelque forme chromatique

qui n'a pas encore eu lieu

qui ne m'attend même pas ---

elle n'a pas besoin de moi ---

mais pour laquelle en partie

je suis là je suis né

l'air danse

oiseaux qui tournent

(cercle circulaire)

archipels

fini de rire

ou de sombrer

pas de différences entre une chose

et son contraire

mais qu'est-ce que le contraire

d'une chose

rien

quelqu'un l'a-t-il effacée ?

qu'est-ce que le contraire

du contraire d'une chose ?

je ne peux rien réparer

je constate la fin des choses

leur destruction

regarde la brume se détacher

doucement

des roches qui forment la colline

de l'autre côté de la route

songe à d'autres collines

où nous avons été heureux

toi et moi

nous ne parlions pas de tout cela

alors

la mort avait déjà fait son devoir

pour moi

mais cela ne faisait rien

que rendre les choses

et la vie et toi

plus intenses

encore

est-ce que tout a changé

ou n'est-ce que ce que

en nous

on a impugné comme toutes

les similivérités

les mensonges les ordres

les injonctions à la discipline

l'obéissance l'obsolescence ?

et puis qu'attendre du temps

sinon qu'il passe ?

avant et après modalités ambidextres

de la succession

rien ne revient mais rien ne disparaît

c'est le même feu qui coule

lave

incendie l'iris incandescent

sens contre sens

guerre au contresens

giclées de foutre

traînées de doute

j'apprends à lire dans le ciel

quand il suce les nuages

que le vent chasse

dehors les chiens aboient

domestiques et les maîtres

s'évertuent à faire des phrases

impossible langage

quand nulle passion ne brûle

profits de la prophylaxie

le cours de l'eau

explose quand les fontaines

toutes sont taries

a-t-on jamais vu visage si triste

couleur si terne vide ?

un instant de silence

enveloppe ce bas monde

quart de la vérité

chacun tente d'expliquer

comme il le peut son attrait

délirant pour l'insensé ou pire

l'à peu près mais personne

ne sait plus parler ou bégaie

comme les onanistes les terroristes

la mitraillette à la main l'air de rien

qui s'étonnera après que

les nymphes se soient suicidées ?

chants d'oiseaux barbares

importés exotiques

rêves de lointains

confins de l'univers

espoirs en série

hôtels clubs camps

concentration des corps

gens d'idéaux bâtards

la nuance tue à la nuance

près les uns des autres

phénomènes entassés

dans l'espoir affaibli que rien

surtout jamais n'arrive

monceaux de déshérence

élans d'assauts hagards

fixe l'x à rabattre

sur tout pourvu que ce soit

comme adam ève

origine discutable quand on y pense

mais à quoi penser ?

tu vois me dis-je

ne restent plus que des questions

plus ou moins mal posées

on a pris l'habitude

de faire des vers sur des affirmations

mais les certitudes les vers

les ont bouffées

ne restent plus alors que ces formes

ambiguës mi-monstre

mi-organe oreilles biscornues

doutes qui se lovent jusque

dans le creux du doute

même amour des deux mains ---

qui pourrait désirer moins ?

le sentiment le plus étrange

regarder le monde autour de soi

ou non même pas le monde

mais tout simplement

ce qu'il y a

faire quelques pas et l'air

qu'on respire est le même

que celui qu'on voit

transparent dis-je encore une fois

pourquoi l'invisible nous semble-t-il

une menace alors qu'il est avant tout

cela même qui nous fait vivre

ce qu'il y a ?

tu vois je ne puis m'empêcher

de poser des questions

est-ce un symptôme

ou une pathologie ?

non

les choses ne peuvent se résumer à cela

tristesse du choix

ou bien ou bien

il faut désirer quelque chose qui

n'existe pas

et si je ne sais comment

c'est le signe peut-être

que quelque chose d'heureux terne moins

est en train d'avoir lieu

quelque chose comme je dis

que je pourrais aimer

quelque chose en vérité

que j'aime déjà

il est tard

je veille

théorie de l'éveil perpétuel

insomnie sans trêve

mais alors comment ferons-nous

pour rêver ?

peut-être qu'à force de ne plus dormir

ou bien de trop

nous finirons enfin par nous défaire

de ce partage

absurde et étroit

entre le sommeil et la veille

n'oublie jamais mon enfant

que c'est la société

qui t'interdit de toucher

les autres que toi

n'oublie jamais mon enfant

que c'est la société

qui détruit

la société

et partout c'est la guerre

où s'étend le regard

s'arrête-t-il par exemple

au bout de la rue

au bout de mon nez

sommes-nous forcés de

nous résumer vendre prostituer ?

tu vois dis-je à quelqu'un

dont je ne connais pas le nom

et qui de toute façon n'écoute pas

tu vois dis-je à tout le monde

le sens ne se fabrique pas

il n'y a pas d'usine pour ça

qui prétend le contraire

ne t'aime pas au contraire

en veut à ton corps à ta peau à ton âme

à ta chair à la chose que tu n'es pas encore mais que tu deviens

ce sens est une histoire d'imagination

il raconte des espaces des horizons

les corps enfermés ne respirent pas

ils étouffent et à la fin

dans l'espoir d'être sauvés

on les retrouve crevés

c'est toujours la même histoire

tant que c'en est triste

tu veux rester chez toi

protégé

cependant que des trains

bondés

se déversent sur l'europe le monde

et tout le monde garde les yeux

fermés

au prétexte de la sécurité

peuples de couards si riches qu'ils en deviennent pauvres ---

c'est-à-dire misérables

vivants mais si morts pourtant

tu vois tu peux me faire

n'importe quoi

c'est vrai après tout

qui suis-je moi

sinon une chose faible seule

fragile

d'habitude je me cache derrière

la horde la société

mais quand elle s'effondre

où puis-je encore m'abriter ?

tu peux me faire n'importe quoi

je ne suis rien qu'indivis

si je disparais tout le monde

bientôt m'aura oublié

d'ailleurs n'as-tu pas déjà commencé

de ne plus savoir qui j'étais ?

les gens forment cohortes

et s'étonnent ensuite qu'on les désagrège

si aisément

tu peux me faire n'importe quoi

je ne cesserai pas d'être moi

tu peux me faire n'importe quoi

moi je ne cesserai pas d'être

parfum de l'air après la pluie

retourner à ce que nous sommes

qui niera par exemple

que nous soyons perdus

que je me sois perdu ?

retour de la somme à moi

à ce que je bois d'elle

je ne coule de rien non

tu vois

je deviens

apparition de la mer

longtemps désertée

iode dans l'air

rien ne vaut le vent

qui bat les cheveux

je me fais des montagnes de vagues

je délire sur un parfum

l'eau noie mes pieds

et moi plongeant les mains dedans

m'en barbouille le visage

et puis lèche avide

tel un animal sciemment privé de vie

le sel qui s'accroche

à mes lèvres enfin libres

un peu après la fin

j'attends

que quelque chose se passe

mais rien

je ne suis pas triste

non pourquoi le serais-je ?

il ne passait rien avant

pourquoi se passerait-il

quelque chose à présent ?

il fait nuit

je sors

respirer quelque chose dehors

le calme apparent

et l'ennui omniprésent

qu'il couve dans sa tranquillité sûre

dépravée

acanthes de nulle part

sauvages

ignorées

besoin de cet abandon

pour se sentir

exister

s'épanouir peut-être

pistils dis-je en outre

avec l'impression

de rendre une manière d'oracle

que je ne comprends pas

pistils dis-je derechef

c'est ici que s'effondrent

les civilisations les empires

sur le bas-côté

halluciné le talus

germinaisons

pétales dédales

globale

effloraison

défilé des paysages

à vive allure entrecoupés

d'allégories de la mort

formes obscures qui émergent

de la terre

même en lumière pleine

elle ne l'exprime pas

plutôt sa négation

sur la route

première sortie depuis

dieu sait quand

le monde semble moins

partagé que jamais

entreprises multiples

illimitées on dirait

de l'ensevelir

sous des montagnes de béton

humaniser la colline

en la colonisant

faire des dépendances

d'arpents de sauvagerie

ce n'est pas le monde

de fait que nous arpentons

mais la vision âcre

que nous avons

rouge à s'en brûler

le regard

friche du monde

allégé

fugue

plus haut la source

canal qui traverse

le pays

eau

je voudrais couler

pas me noyer

non

flux

devenir liquide

iridescent

sang bleu blanc

mi-eros mi-violent

tout au bout sans goutte

flot

l'océan

ce que dans mon jargon prosaïste

j'appelle d'une femme mot

la mer la mer

le problème de l'élan

me dis-je

c'est qu'un instant

il s'arrête

plus de mouvement

quand moi je le sais

pourtant je voudrais continuer

peine perdue que d'en chercher

un nouvel

quelque impulsion

pour devancer la machine

il n'y a pas de machine

rien que cette chose que moi

je suis

et nomme moi

plus d'élan

des manières d'avancer sans

toute recherche tend

vers l'impossible

elle et son contraire

la découverte

sans le dire

je fais de l'étymologie

sauvage

invente dans le langage

des arts de ne m'épuiser pas

des efforts qui n'ont pas besoin de dire

leur nom

ils sont ---

non mieux

ils font

mer plate

quand le tonnerre gronde

et que l'orage menace

mais tout est si paisible

en face

sur l'île ---

qui a dit qu'il fallait toujours

chercher à dépasser

les contradictions ?

au fond du paysage

un point de doute

par où les sentiments

si nous en avions

pourraient s'enfuir

prisme des sensations

malgré nos notions toujours

plus confuses

à l'autre bout de la chaîne

contestataire j'évoque

les raisons de croire encore

en quelque chose mais

a-t-on jamais vu ma chère

populace si rare si frêle ?

c'est le désert

aurait-on envie de s'écrier

mais non le désert

c'était avant

et à présent moi

je cherche un nom un verbe

une phrase

pour dire quelque chose

après

retarde délaie

rumine et pars

cherche la rime infime

infracrépusculaire

matière des corpuscules

ils avancent dans le noir

rougeoie colore-toi

blême où vas-tu ?

ne singe pas

ne fais pas signe ni société ni peuple ni rien

sois

quelque chose ou

rien

nul ne va jamais si loin que ça

ici ou là

façon de faire semblant de faire à sa façon

quand nenni

turbulences plutôt

perturbations plutôt

quête insensée mendiant policé

à force de prétendre nous avons oublié

d'entendre la voix

vent dans les feuilles de l'arbre

mouvements des animaux

oiseaux

pas de destin

ne crois pas cela non

pas de destin ---

attention

éclairs dans le ciel

tout brille certes

mais tout a déjà brûlé

qui suis-je moi ?

demande-le-moi

une braise un songe

un peu de poussière

le père ou la mère dieu

une théorie dont on s'entiche

la grande sœur de la vérité

cassandre muette assassinée

une érection dans la mollesse

un gland châtré

de la chasteté l'espérance

dis-le-moi

fais-je des ronds avec mes doigts

dans le néant

exercices circulaires

pour qui n'a plus rien

à faire ou est-ce que je

dérive sans direction

pain de glace sans océan ?

je tends les mains

y a-t-il un sol une terre

où m'attacher ?

je tire un trait ---

ne me dis pas moi

que je suis

non aide-moi

à m'oublier

à ignorer que jamais

non rien ne dis

rien

soif immorale

de passions aphrodites

les autres se font

des frissons de que dalle

s'imaginent des drames

qui n'existent pas

mais moi quand je te regarde

je vois la chair dans le marbre

les corps qui dansent

sans halte

les parfums parthénopes

et les odeurs de feu

le bois de la déesse

et le silence en bas

j'essaie de me poser une question

que je ne trouve pas

me souviens de l'argent

que nous n'avions pas

de tout ce qui se perd

dans les rues de naples

qu'on l'aime ou

qu'on ne l'aime pas

la ville aux millions

de pas

l'amour est là

un jour de plus

égale

un jour de moins

ici

toit de pierres plates

rien qui le regard

arrête

abstraite manière de voir

les formes

où suis-je ?

dans quel pays transporté

nature jaune des innombrables

soleils

immortelles

dit-il

et je l'écoute

pensant à des déesses

nichées dans des abris de fortune

des abris nucléaires

attendant époques plus propices

qui probablement

ne reviendront jamais

y pensant

comment puis-je

me dis-je

ramener l'affaire mythologique

à une question de statistique ?

tu sais

en guise de réponse alors

j'ai beau tout faire

pour ne pas l'aimer

je ne suis jamais

que l'enfant de mon époque

et je ne l'appellerai pas

maman non mais

ma chère vieille mère

comment l'on dit dans la phrase

ma chère vieille mère

quand allez-vous enfin

vous décider à crever ?

stylite styliste

retour à la ligne

en haut de la colonne

la tour

site sur le toit du monde

sis sans état

comme tous les saints

styliste stylite

sur terre l'air béat

yeux levés vers on ne voit où

le ciel le plafond

un interstice par où

passe la lumière

le sens

la science des altitudes

et des mesures

des plaines ouvertes

grandes à mes pieds

art du surplomb

pour qui ne manque pas

d'aplomb de suite

dans les idées

art de faire chanter les plantes

sauvages en marge

de l'autoroute

moteur blanc

âme maussade

un vers un végétal

articulations sans ligaments

il y a un point à l'horizon

qui ne dit pas son nom

je suis perdu

peut-être

mais il y a quelque chose

de plus que ce là-là

j'ai beau revenir

sur mes pas

j'avance

spirale du labyrinthe

géométrie sans destin

ou un

que je ne comprends pas

partout autour

de moi

il y a des hommes

insulte aux lèvres

qu'est-ce qui les pousse à être si laids ?

je les entends

qui crient

quand même je ne comprendrais pas

ce qu'ils disent

partout autour

de moi

il y a des hommes

pourquoi sont-ils

si laids ?

je regarde ailleurs

un peu plus haut

je crois

je vois des couleurs

j'entends des rêves

les morts râlent leur vie

dans les époques sombres

y en eut-il d'autres

jamais ?

je voudrais en dessiner

les peindre

mais comment faire voir

les couleurs

à un peuple de daltoniens ?

la beauté du monde

et la laideur

de l'immonde

si proches en fait

qu'elles se touchent

presque

quand tu traverses

passes de l'une à l'autre

sans hiatus

un rictus oui

trop même

plisse les yeux

ne regarde pas au-delà

mais dedans

en plein

il ne faut pas avoir peur

de la mort

de l'existence de la laideur

peut-être que non

que toute la réalité

du ciel ne s'épuise pas

ici dans cette succession

de succion réflexe

d'un phénomène par ce

qui le nie

mais il te faut quand même

traverser le pays

tout entier traverser

la vie

image immobile

où la vision

dessine le pays sans halte

l'absence de repos

herbes jaillies du creux

du bitume

dans l'amertume ---

trous

folie propre à ce qui passe

image ?

non mobile

ouvert à tout

réception maximale

pas passive

pas dans les manques

où l'être fait défaut

devenir

je ne me soucie pas

de qui se suffit de gésir

humanoïdes connectés

cerveaux débranchés

végétative vie

contrainte

pas végétale

éteinte

le contraire

dans mon ouverture optimale

je suis étranger à tout

ne suis étranger à rien

en quoi l'inverse de l'inverse

m'empêcherait-elle

d'exister de croître

de n'être pas comme l'être

identique mais

de me déployer ?

opale

visage taches

de sang dans

la verdure

pays avant qu'il ne soit

calciné

parfois j'oublie

de regarder la route

je pense à autre chose

des idées de rien

la courbure du temps

de tes fesses

reflets dans le miroir

opale

image non

réel là sans

hypnose ni glose

éclair du naturel

la lumière me dis-je

trouve toujours

le bon moyen

de s'exprimer

je jette encore un regard

t'embrasse

où ai-je passé

la dernière éternité ?

est-ce que l'une appelle

quand l'autre sent

ou réciproquement ?

qui fait semblant

de jouir

les poings liés

les pieds entravés

il y a quelqu'un

qui se promène

sauf qu'il n'a pas

de nom

appels de phares

dans le rétroviseur

affaires d'hommes

pressés

histoires d'être

un peu plus

angoissé

je regarde la ligne

de démarcation

entre ici et nulle part

ou je ne sais où

c'est vrai qu'il m'arrive

souvent de faire

semblant

regarder au-delà

détourner le voir

personne ne me l'a demandé

non

c'est une habitude

façon détestable

de faire seconde nature

alors quoi ?

je ne sais pas

peut-être je devrais

me taire

ou bien dire

ça suffit

je quitte des yeux

la route

un instant

de trop

et le décor clos

devient comme

une seconde peau

pas question

de prendre

l'eau

quand vivre

est une interdiction

partout c'est

la guerre

cadastre

gerbes

aventures

sur la devanture

qui ne fabrique pas

petite boutique

son être-là véridique ?

bêtes malades

lancées à tombeau ouvert

sur l'autoroute

hystérique motocycle

mais regarde

et ose appeler cela désir

et ose appeler cela vivre

gerbes

cimes en extase

comme je dis

par antonomase

fais-moi jouir

amour

le taureau est un homme

comme les autres

et réciproquement

qui pourrait dire ce qu'il se passe

au centre du labyrinthe

au centre du moi

à moins d'y avoir mis les pieds

soi-même

et d'en être ressorti

vivant et puissant ?

fais-moi jouir

n'arrête pas

avant que j'aie rendu

mon dernier souffle

gerbes

non fleurs

les ruses fusent

à quelle vitesse va

la vérité ?

et le faux

lourdeur sans altitude

nous entraîne-t-il

nous appesantit-il

toujours un peu plus bas

toujours un peu plus las ?

je trace un cercle

avec les yeux

tout autour de moi

des astres scintillent

idées ou bien couleurs

manières de s'arrimer

au monde

et puis de défaire

tous les liens

qui peut encore

vouloir dire le vrai ?

que peut encore

vouloir dire le vrai ?

je fais un nœud

autour de mon cou

avec une langue

ou bien j'étouffe

ou bien je découvre

le bon moyen de m'exprimer

abjects objets

aux rites arides

pas d'air entre nos rythmes

frénésie du dispositif

pas de mer entre nos récifs

même les os sonnent creux

peureux

tout coule

je fixe un point blanc

un peu trop longtemps

et oublie

je revis

est-ce que je peux faire semblant

que je suis là

que je participe

et m'absenter

en revanche de cette réalité

bonne qu'à me haïr

et la détester en réponse ?

est-ce que je peux m'absenter

derrière le cache

des apparences

donner le change

à qui le veut

et trouver ailleurs

une ombre de paix ?

je ferme les yeux

les oreilles non

sons diffus dans l'atmosphère

souffles d'air

mélanges qui ne laissent pas d'être

étranges

entre un moteur à explosion

et le chant d'un oiseau

(espèce invasive)

et moi ?

il n'y a pas de mais

je ne le retiens pas

garde mon souffle

aucune parole ne sort

de ma bouche

apnée de l'oreille

idem

à force de tout entendre

on n'écoute plus rien

ou l'inverse

je ne sais pas très bien

je contemple la grisaille

du temps et du temps

toute l'eau du monde

dégouline sur mes pieds

nus

je compte les gouttes

une à une et très vite

très vite ne sais plus

combien

des milliards probables

partout sur la terre

déluge et sécheresse

vivre cette contradiction

n'est-ce pas faire

l'expérience d'être vivant ?

peut-être bien ou alors non

je ne dis rien

n'est-ce pas certain de toute façon

qu'il n'y a personne

personne pour m'entendre

non plus ?

si je m'arrête

est-ce que je meurs ou bien est-ce que

je flotte sans contraintes

sans contraires

autres que l'allure immobile

que je ferais mienne ?

si je m'arrête est-ce que le vent

va me souffler

dans le lointain ?

deviendrais-je nuage

léger comme un peu

de fumée ?

si je m'arrêtais

qui deviendrais-je

au repos volontaire sinon

une image figée

chose par son absence de force ?

où est le principe de mon mouvement

dans l'air du temps ?

je ne vais pas m'arrêter

la poitrine légère

les yeux dans le néant

je vais continuer

qui peut m'arrêter ?

entre les orteils

des continents

inexplorés ---

pourquoi ai-je pensé

cette phrase ?

pourquoi me la suis-je

dite à moi-même

et pourquoi l'ai-je

écrite ?

à force de marcher

peut-être

toujours question d'avancer

mais pour aller où ?

cela qui le sait

qui le sait

sinon mes pieds ?

partout où

il y avait une beauté

une beauté possible

un pas en avant

dans le désert

il y a le nom

d'une marque

et elle a un prix

affiché grand

c'est la vie

partout où

il pouvait y avoir une chance

d'éclaircie et de métamorphose

on l'a baptisée

réduite à un nom propre

chose lequel n'est pas le sien

mais de n'importe qui

de n'importe quoi

le nom d'une valeur

laquelle enveloppe le monde

tout autour

et une entière

jusqu'à la fin

de tout

et partout

déclare-t-on

partout

c'est la guerre

comment se mesure l'espace

entre l'espace et l'espace

le peut-il seulement ?

et quand il pleut

se dilue-t-il ?

je ne sais pas

c'est vrai que je ne sais pas

si c'est le bruit de la pluie

ou l'arrosage automatique

et par suite

quel sens donner à

l'éternel retour

si c'est la vérité des saisons

ou la fable d'un charlatan ---

aussi j'écoute l'enfant

elle qui sait et me répond

sans que je l'interroge et

regardant le ciel dit-elle

tu vois quand tout sera fini

tout reviendra

tout redeviendra ruines

et les égyptiens et les grecs

et toi alors

et toi aussi mon amour ?

oui

et tout

à l'infini

vieille

comme une âme sans âge

à qui parler sinon

autant se taire non ?

mon nihilisme a des accents

rageurs

comment sauver le monde

et le faut-il seulement ?

tout le monde a quelque chose

à dire ou réclamer ou revendiquer

une morale à faire à l'autre

seul mode d'affaire à tous

une haine des siècles rentrée

en chacun

et qui surgit soudain

personne ne sent plus heureux

certains plus légers

cependant

la tête en moins

qui a roulé au loin

casanova dans la préface à l'histoire de sa vie

ironise ainsi

vive la république

il est impossible

qu'un corps sans tête

fasse des folies

hier comme aujourd'hui

comment pense

qui n'a pas l'esprit ?

soi-même comme

une langue étrangère

soi-même dans

une langue étrangère

s'épuiser dans l'amour

et puis ne plus rien comprendre

les agents agissent

et moi la bouche bée béate

est-ce que j'essaie de savoir

ou simplement de survivre ?

les agents agissent

qu'ont-ils d'autre à faire ?

exister n'est pas une raison d'être

de continuer

faire semblant non plus

on joue à vivre mais

au fond c'est le grand

mensonge

quiconque

croit tenir une idée

s'imagine en devoir de l'exprimer

il y a tant d'essences

et si peu d'air

à respirer

combien

combien de temps

encore

à expirer ?

je renie tous les poèmes

aussi bien que tout

tout ce que j'aime

j'annule les rendez-vous

avec moi-même l'être

et son destin

je me fabrique des chevaux

de bataille

en haut desquels se jucheront

des nains

ivres et hagards

j'habite des idéaux bâtards

là-haut tout en haut de la montagne

froide et hostile

où se sont réfugiés

les dieux objets

de nos désirs objets

de nos haines de sujets

ils ont peur de nous

et de nos sirènes hurlantes

elles qui déchirent la nuit

moteurs à trop de temps

explosions nocturnes

allumés pour détruire la nuit

la première nuit de l'été

fraîche et de chair

ce matin

n'avais-je pas entendu

une cigale enfin ?

insecte au lyrisme spontané

pour qui ignore désormais

pourquoi il est

pour quoi il est

né

ciel rouge

sanguine clandestin

je décèle dans des lignes imaginaires

l'ironie la guette

qui décille

cris d'oiseaux

ou bien de l'enfant

il y a un intrus

dans la maison

le sommeil pleure

que faire ?

le réel est une machination

en plein rêve

tremblement de terre

ou la mer échoue

déjoue

nos stratagèmes

vulgaires

quelque part il y a quelqu'un

qui réussit mais

ce n'est pas moi ce n'est pas moi

il faut écrire pour dépasser

la rature

ou l'inventer c'est-à-dire

quand le ciel rouge

se couvre de nuages

digitaux

bites en bas lourds

et que la bêtise est

notre unique destin

sueur sur la joue

gouttes

les cheveux dans la bouche

goûte

j'imagine le ciel

sans le regarder

mais je ne me le représente pas

non

je le laisse exister

comme j'aimerais

je crois

qu'on me laissât

exister

et l'apparition d'un temps

désuet

impropre certainement

de quoi est-elle l'effet

de la chaleur

ou de son ridicule

qu'on appelle canicule ?

sueur sur la joue

et jusques en la bouche

je la goûte

salée

façon de dire

sans doute que c'est

l'été

solitude du bleu

y entrer

faste astre du souvenir

langue réminiscente

languissante

roches dures tendues

au ciel

vivants d'hier

il y a dix mille mille ans

femmes et hommes

couples et copules

la phrase un sexe

et le lien liquide

bruns corps de soleil

sur la pierre blanche

sentence lapidaire

éblouissante

tous nos rites sont barbares

impavides les plantes

et lors le vide

des fleurs jaunes

capitulent

parfum capiteux

de l'authenticité

tous les arguments sont

captieux

rien ne vaut la paix

de l'âme

imaginations en forme

de mascarades

c'est vrai

quand on y pense

que la vie mérite mieux

que ça

mais qui pense

qui ne fait pas semblant

qui désire encore

l'anéantissement ?

le soleil ne brûle pas

mais il n'y a pas d'air

dans l'air

rien que le dégagement

d'apparences lucides

claires

pour qui veut bien

les voir

une seconde de plus

en apnée

et j'atteindrai à l'apogée ---

pas de style

l'esprit limpide

désert tout en nuances

la chance appartient

à qui veut bien l'ignorer

admiration du rouge

quand tout aura disparu

tache au milieu du champ

voir

d'abord les yeux fermés

brûlure à même les rétines

rétives

j'imagine des peaux claires

mais c'est la mienne

que je vois

les trottoirs sont peuplés

de sauvages

mais les urnes de même

ainsi ne fais-je jamais que passer

outre le chant

le doux nom

de démocratie

cannibalisme blême

les ténors sont de sortie

les peintres aussi

je fixe

l'immaculée macula

de l'enfant que j'espère

bien née

les femmes sont

le devenir des gommes

commençons donc

par tout

effacer

quel signe faire

quel geste de la main

avoir quelque chose à voir

ou alors rien ?

il y a toujours quelqu'un

qui fait semblant

et se refuse à garder le silence

et moi qui n'ai pas grand-chose

même pas un gramme de science

des traces peu

que je disperse

y a-t-il un monde

au coin duquel

me tenir

ou bien tout ceci n'est-il jamais que

la répétition

d'un drame antique

et profond

auquel cependant

faute de sens et par la nôtre

nous n'entendons

que du souffle

une langue étrangère ?

même pas ---

que du vent ---

n'est-il pas semblable

à cela notre destin ?

d'une intelligence certaine

mais que nous ne saisissons pas

et d'espaces désirables

dans le temps de notre vie

ne restent qu'échos

lointains

la certitude de la défaite

qui ne vaut pas

mieux que de la victoire

je forme des phrases certes

avec des mots que personne

n'entend et les phrases dites

de même

aphones et profanées

mais pas assez

non il faut croire

pas assez pour me faire en silence

une promesse à moi-même

de n'écrire plus jamais

que la formule

ultime qui résoudra tout

en une cadence parfaite

sans nulle autre au-delà

le dernier aphorisme

j'écris des poèmes

je trace des figures

dans le ciel

que personne ne voit

que personne ne regarde

en bas que du bruit

pourquoi hésiterais-je

à exister ?

pas de halte

ailleurs qu'ici

je regarde le bout de mes doigts

quelque chose brille

il y a toujours un sens

à découvrir ou inventer

une mémoire à fabriquer

rien n'est donné

de bon tu sais

que les mensonges

et les assassinats

rien n'est donné

d'aimable tu sais

que les haines séculières

vengeresses

tout le monde s'entreregarde

mais personne n'a la moindre idée

de quand il faudra

arrêter

qu'affaires

doxa du monde mien

les œuvres de l'esprit

dicte

en lettres capitales

le catéchisme de l'époque

les œuvres de l'esprit

ne changeront plus le cours

du monde

plus rien à faire dès lors

que nous abandonner

dans la consomption de nous-mêmes

dans l'admiration de nous-mêmes

sorte de destruction

en miroir des mythes anciens

sur quoi s'écrasent

désormais les langues

pesantes

mortes avant d'avoir existé

éternité précaire

soliloque de chacun

et personne pour tous

nous fabriquons du divers

insensible et sans lendemain

ce n'est pas que je pleure

non

c'est que je m'ennuie

et partout

c'est la guerre

je préfère moins faire

attendre

gratter une surface

molle

on rit des heures frivoles

mais qui sait quand

elles s'arrêtent

et rond

j'attends

est-ce que je sens quelque chose ?

une différence

l'aspect louche

d'une vérité première

je pourrais regarder

ailleurs

faire un pas de côté

si seulement je savais

pour quoi nous sommes

nés

les autres les gens

tous ceux-là

que je ne comprends

pas préfèrent encore

s'enterrer

je ne suis pas là

pour les juger

mais une vie pareille

qui pourrait seulement

la désirer ?

le destin est aveugle

non pire

il est muet

reflets dans la baie

vitrée

le monde voit double

grande roue ensanglantée

des âmes vivantes

encore un peu si peu qui peut

faire semblant d'aimer

les choses telles qu'elles sont

mais quelles choses sont

telles qu'elles sont ?

la roue tourne

c'est l'évidence sur elle-même

qui pourrait nier que tout

revient

mais il suffirait de couper le courant

l'électricité

pour que l'éternité

ne se résume enfin

qu'à ce qu'elle est

une manière un peu simple

un peu vulgaire

d'ignorer la fin

tel que je le vois

moi

l'univers clignote

et ce n'est pas

loin de là

pour son plus grand bien

la fin de christophe colomb

fallait-il laisser notre conscience

politique à d'autres ?

échos dans la mort

héros le silence

bulles d'échec

de vérité de science

silencieux

le mâle zêta

au-dessus contemple

les faillites du vivant

oh oui bien sûr je pourrais

croire n'importe quoi et

faire croire n'importe quoi

le visage des choses

change

avec le maquillage

je me souviens

d'une marche

que nous avions faite

ensemble

vers le sommet

le monde en effet

paraissait plus petit

au retour pourtant

rien n'avait changé

mais nous faisons

si bien semblant

que le mouvement

semble réel et

son absence suspecte

moi je me tais

et patiente

en attendant

la prochaine excursion

peu d'astres

dans le ciel

mais des satellites

giratoires grégaires

histoire de faciliter

la vie sur terre

le ciel est un trou

béant si on le fixe

longtemps on voit l'infini

dedans il paraît

qui n'a pas les yeux

épuisés de scrutation

de sensations ?

répertoire des formes fixes

quand tout est en mouvement

même si l'on voulait

s'arrêter

on échouerait d'une chute passable

il n'y a plus de repos

passé le premier élan

pas de théories ---

des avancées

dans le noir

apprends à contempler le mal

en silence
