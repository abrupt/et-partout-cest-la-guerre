# ~/ABRÜPT/JÉROME ORSONI/ET PARTOUT C’EST LA GUERRE/*

La [page de ce livre](https://abrupt.cc/jerome-orsoni/et-partout-cest-la-guerre/) sur le réseau.

## Sur le livre

je n’ai pas de mémoire —<br>
zéro octet —<br>
j’ai tout oublié<br>
et ne veux me souvenir de rien<br>
sang<br>
mort<br>
ordure anecdotique<br>
métaphore<br>
autorité<br>
punition<br>
châtiment<br>
honte<br>
culpabilité<br>
je veux qu’on ne me rappelle rien<br>
l’innocence c’est moi

## Sur l'auteur

Jérôme Orsoni est écrivain. [Ses cahiers fantômes](https://cahiersfantomes.com/). Le reste n’ayant que peu d’importance.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
